package pt.ipp.estg.peddypapper.components.controllers

import android.util.Log

/**
 * Helper function that checks if the user's answer is correct.
 *
 * @param userAnswer User's answer.
 * @param correctAnswer Correct answer.
 * @return True if the user's answer is correct, false otherwise.
 */
fun checkAnswer(userAnswer: String, correctAnswer: String): Boolean {
    val isCorrect = userAnswer.equals(correctAnswer, ignoreCase = true)
    return if (isCorrect) {
        Log.d("tag", "correu tudo bem")
        true
    } else {
        Log.d("tag", "errei a pergunta")
        false
    }
}