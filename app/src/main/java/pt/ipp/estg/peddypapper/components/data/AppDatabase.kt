package pt.ipp.estg.peddypapper.components.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import pt.ipp.estg.peddypapper.components.models.PlayerInformation
import pt.ipp.estg.peddypapper.components.models.Question

@Database(entities = [PlayerInformation::class, Question::class], version = 4)
abstract class AppDatabase : RoomDatabase() {
    abstract fun playerDao(): PlayerDao
    abstract fun questionDao() : QuestionDao

    companion object {
        private const val DATABASE_NAME = "peddypapper-db"

        private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    DATABASE_NAME
                ).fallbackToDestructiveMigration().build()

                this.instance = instance
                instance
            }
        }
    }
}