package pt.ipp.estg.peddypapper.components.composable

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Send
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewmodel.compose.viewModel
import pt.ipp.estg.peddypapper.R
import pt.ipp.estg.peddypapper.components.controllers.checkAnswer
import pt.ipp.estg.peddypapper.components.data.AppViewModel
import pt.ipp.estg.peddypapper.components.models.Answer
import pt.ipp.estg.peddypapper.components.models.Question

/**
 * Component that displays the question and the input field of the question.
 */
@Composable
fun QuestionDisplayForm(
    playerId: Int,
    notNullQuestion: Question,
    changeQuestionBackgroundColor: (Boolean) -> Unit,
    wasAnswerToQuestionCorrect: (Boolean) -> Unit,
    questionDifficulty: (String) -> Unit,
    radioOptions: MutableState<List<String>>
) {
    var userAnswer by remember { mutableStateOf("") }

    var isAnswerCorrect by remember { mutableStateOf(false) }

    var wasQuestionAnswered by remember { mutableStateOf(false) }

    val appViewModel: AppViewModel = viewModel()

    if (wasQuestionAnswered) {
        LaunchedEffect(true) {
            appViewModel.insertAnswer(
                Answer(
                    0,
                    playerId,
                    notNullQuestion.question,
                    userAnswer,
                    notNullQuestion.correctAnswer,
                    isAnswerCorrect
                )
            ).asFlow().collect {}
        }
    }

    Column(
        modifier = Modifier
            .padding(all = 16.dp)
            .fillMaxWidth(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(R.string.question_questionLabel) + " ${notNullQuestion.id}: ${notNullQuestion.question}",
            style = MaterialTheme.typography.bodyMedium
        )

        Log.d("QuestionDisplayForm", "Question: $notNullQuestion")

        Column(
            modifier = Modifier
                .padding(top = 16.dp)
                .fillMaxWidth(),
        ) {
            radioOptions.value.forEach { answer ->
                Row(
                    modifier = Modifier
                        .padding(top = 16.dp)
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    RadioButton(
                        selected = userAnswer == answer,
                        onClick = {
                            userAnswer = answer
                        }
                    )

                    Text(
                        text = answer,
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
            }
        }

        Row(
            modifier = Modifier
                .padding(top = 16.dp)
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = stringResource(R.string.question_difficultyLabel) + ": ${notNullQuestion.difficulty[0].uppercaseChar() + notNullQuestion.difficulty.substring(1)}",
                style = MaterialTheme.typography.bodyLarge,
                modifier = Modifier.padding(top = 16.dp),
                color = when (notNullQuestion.difficulty) {
                    "easy" -> Color.Green
                    "medium" -> MaterialTheme.colorScheme.primary
                    "hard" -> MaterialTheme.colorScheme.error
                    else -> Color.Black
                }
            )


            Button(
                onClick = {
                    isAnswerCorrect = checkAnswer(userAnswer, notNullQuestion.correctAnswer)
                    questionDifficulty(notNullQuestion.difficulty)

                    if (isAnswerCorrect) {
                        wasAnswerToQuestionCorrect(true)
                        changeQuestionBackgroundColor(true)
                    } else {
                        wasAnswerToQuestionCorrect(false)
                        changeQuestionBackgroundColor(false)
                    }

                    wasQuestionAnswered = true
                },
            ) {
                Icon(Icons.Default.Send, contentDescription = null)
                Spacer(modifier = Modifier.width(4.dp))
                Text(text = stringResource(R.string.question_sendButtonLabel))
            }
        }
    }
}

