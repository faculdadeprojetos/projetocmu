package pt.ipp.estg.peddypapper.components.models

data class LeaderboardItem(
    val correctAnswers: Int,
    val wrongAnswers: Int,
    val score: Int,
    val level: Int,
    val name: String,
)
