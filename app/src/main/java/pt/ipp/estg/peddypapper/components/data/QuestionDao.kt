package pt.ipp.estg.peddypapper.components.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import pt.ipp.estg.peddypapper.components.models.Question

@Dao
interface QuestionDao {

    /**
     * Function to get all question information from firebase - Might not be used
     */
    @Query("SELECT * FROM Question")
    fun getAllQuestions(): LiveData<List<Question>>

    /**
     * Function to get question information by id from firebase
     */
    @Query("SELECT * FROM Question WHERE id IN (:questionId)")
    fun getQuestionById(questionId: Int): LiveData<Question>

    /**
     * Function to insert question information into local cache
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertQuestion(question: Question)

    /**
     * Function to get the number of questions in local cache
     */
    @Query("SELECT COUNT(*) FROM Question")
    suspend fun getQuestionCount(): Int

    /**
     * Function to update local cache of question information
     */
    @Update
    suspend fun updateQuestion(question: Question)

    /**
     * Function to delete local cache of question information
     */
    @Delete
    suspend fun deleteQuestion(question: Question)
}
