package pt.ipp.estg.peddypapper.components.composable

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.net.Uri
import android.telephony.SmsManager
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import kotlinx.coroutines.flow.collect
import pt.ipp.estg.peddypapper.R
import pt.ipp.estg.peddypapper.components.data.AppViewModel
import pt.ipp.estg.peddypapper.components.models.PlayerInformation
import kotlin.math.pow

@Composable
fun PlayerProfile(
    player: PlayerInformation,
    navController: NavController,
    configurationOrientation: Int,
    changeCurrentUser: (PlayerInformation?) -> Unit
) {
    var setPhone by remember { mutableStateOf("") }
    var setText by remember { mutableStateOf("") }
    var isPhoneDialogVisible by remember { mutableStateOf(false) }

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.primary)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {
            // Imagem do Perfil
            Image(
                painter = painterResource(id = R.drawable.ic_launcher_background),
                contentDescription = null,
                modifier = Modifier
                    .size(110.dp)
                    .clip(CircleShape)
                    .background(MaterialTheme.colorScheme.primary)
            )

            Row {
                // 2 buttons here
                Button(
                    onClick = {
                        navController.navigate("answerHistory")
                    }, modifier = Modifier
                        .weight(1f)
                        .padding(6.dp)
                ) {
                    Text(text = stringResource(R.string.playerProfile_answerHistory))
                }

                Button(
                    onClick = {
                        navController.navigate("editProfile")
                    }, modifier = Modifier
                        .weight(1f)
                        .padding(6.dp)
                ) {
                    Text(text = stringResource(R.string.playerProfile_editProfile))
                }
            }

            Button(
                onClick = {
                    changeCurrentUser(null)
                    navController.navigate("login")
                },
                modifier = Modifier
                    .padding(6.dp)
                    .align(alignment = Alignment.CenterHorizontally)
            ) {
                Text(text = stringResource(R.string.playerProfile_logout))
            }


            // Nome do Jogador e Nickname
            Row(
                modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Column {
                    Text(
                        text = player.playerName,
                        style = MaterialTheme.typography.headlineLarge.copy(fontWeight = FontWeight.Bold)
                    )

                    Spacer(modifier = Modifier.height(4.dp))
                    Text(
                        text = player.nickname, style = MaterialTheme.typography.titleSmall
                    )

                    Spacer(modifier = Modifier.height(4.dp))
                }
            }

            Spacer(modifier = Modifier.height(15.dp))

            Text(
                text = player.description, style = MaterialTheme.typography.bodyMedium
            )

            Spacer(modifier = Modifier.height(15.dp))

            Row(
                modifier = Modifier
                    .padding(vertical = 8.dp)
            ) {
                IconButton(onClick = { isPhoneDialogVisible = true }) {
                    Icon(
                        imageVector = Icons.Default.Phone,
                        contentDescription = "Localized description",

                        )

                }
                if (isPhoneDialogVisible) {
                    PhoneInputDialog(
                        onConfirm = { phoneNumber, messageText ->
                            setPhone = phoneNumber
                            setText = messageText
                            sendSms(phoneNumber, messageText)
                        },
                        onDismiss = {
                            isPhoneDialogVisible = false
                        }
                    )
                }

                Spacer(modifier = Modifier.width(10.dp))

                Text(
                    text = player.phoneNumber.toString(),
                    style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold),
                    fontSize = 12.sp,
                    modifier = Modifier.weight(1.5f)
                )

            }


            if (configurationOrientation == Configuration.ORIENTATION_LANDSCAPE) {

                Row(
                    modifier = Modifier.fillMaxWidth(),
                ) {
                    Spacer(modifier = Modifier.width(10.dp))


                    Column(
                        modifier = Modifier.weight(1f)
                    ) {


                        Spacer(modifier = Modifier.height(20.dp))

                        InfoItem(
                            symbol = "✔",
                            title = stringResource(R.string.playerProfile_correctAnswers),
                            value = player.correctAnswers.toString()
                        )
                        Spacer(modifier = Modifier.height(20.dp))


                        InfoItem(
                            symbol = "❌",
                            title = stringResource(R.string.playerProfile_wrongAnswers),
                            value = player.wrongAnswers.toString()
                        )

                        Spacer(modifier = Modifier.height(20.dp))

                        InfoItem(
                            symbol = "\uD83D\uDCD6",
                            title = stringResource(R.string.playerProfile_totalQuestionsAnswered),
                            value = player.totalQuestionsAnswered.toString()
                        )

                        Spacer(modifier = Modifier.height(20.dp))


                    }

                    Spacer(modifier = Modifier.width(25.dp))

                    Divider(
                        color = Color.Black,
                        thickness = 1.dp,
                        modifier = Modifier
                            .height(170.dp)
                            .width(2.dp)
                    )

                    Spacer(modifier = Modifier.width(35.dp))

                    Column(
                        modifier = Modifier.weight(1f)
                    ) {
                        InfoItem(
                            symbol = "⚙️",
                            title = stringResource(R.string.leaderboard_level),
                            value = player.gameLevel.toString()
                        )

                        Spacer(modifier = Modifier.height(35.dp))

                        InfoItem(
                            symbol = "🎮",
                            title = stringResource(R.string.playerProfile_experience),
                            value = player.gameExperience.toString() + "/" + ((100 * 2.0.pow((player.gameLevel - 1)))).toInt()
                                .toString()
                        )


                        Spacer(modifier = Modifier.height(35.dp))

                        InfoItem(symbol = "🏆", title = "Score", value = player.score.toString())
                    }
                }
            } else if (configurationOrientation == Configuration.ORIENTATION_PORTRAIT) {
                Column(
                    // Make it scrollable
                    modifier = Modifier
                        .fillMaxWidth()
                        .verticalScroll(rememberScrollState())
                ) {
                    Spacer(modifier = Modifier.height(10.dp))

                    InfoItem(
                        symbol = "✔",
                        title = stringResource(R.string.playerProfile_correctAnswers),
                        value = player.correctAnswers.toString()
                    )

                    Spacer(modifier = Modifier.height(20.dp))

                    InfoItem(
                        symbol = "❌",
                        title = stringResource(R.string.playerProfile_wrongAnswers),
                        value = player.wrongAnswers.toString()
                    )

                    Spacer(modifier = Modifier.height(20.dp))

                    InfoItem(
                        symbol = "\uD83D\uDCD6",
                        title = stringResource(R.string.playerProfile_totalQuestionsAnswered),
                        value = player.totalQuestionsAnswered.toString()
                    )

                    Spacer(modifier = Modifier.height(20.dp))

                    InfoItem(
                        symbol = "⚙️",
                        title = stringResource(R.string.leaderboard_level),
                        value = player.gameLevel.toString()
                    )

                    Spacer(modifier = Modifier.height(20.dp))

                    InfoItem(
                        symbol = "🎮",
                        title = stringResource(R.string.playerProfile_experience),
                        value = player.gameExperience.toString() + "/" + ((100 * 2.0.pow((player.gameLevel - 1)))).toInt()
                            .toString()
                    )

                    Spacer(modifier = Modifier.height(20.dp))

                    InfoItem(symbol = "🏆", title = "Score", value = player.score.toString())

                    Spacer(modifier = Modifier.height(20.dp))


                    InfoItem(symbol = "👑",
                        title = "Clan",
                        value = "${player.clanId.toString()}  <- ",
                        onClick = {
                            navController.navigate("DisplayClanById/${player.clanId}")
                        })


                }
            }
        }
    }
}


@Composable
fun InfoItem(symbol: String, title: String, value: String, onClick: () -> Unit = {}) {
    Row(modifier = Modifier
        .clickable { onClick() }
        .padding(vertical = 8.dp)) {
        Text(
            text = symbol,
            style = MaterialTheme.typography.bodyMedium,
            fontSize = 16.sp,
            modifier = Modifier.weight(0.5f)
        )


        Spacer(modifier = Modifier.width(10.dp))

        Text(
            text = title,
            style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold),
            fontSize = 12.sp,
            modifier = Modifier.weight(1.5f)
        )

        Spacer(modifier = Modifier.width(10.dp))

        Text(
            text = value,
            style = MaterialTheme.typography.bodyMedium,
            fontSize = 15.sp,
            modifier = Modifier.weight(1f)
        )
    }
}


/**
 * Gets a player by its id.
 *
 * @param id Id of the player to get.
 *
 * @return PlayerInformation object with the player information.
 */
@Composable
fun getPlayerById(id: Int): PlayerInformation {
    val appViewModel: AppViewModel = viewModel()

    val player = appViewModel.getPlayerById(id).observeAsState()

    player.value?.let {
        return it
    }

    return PlayerInformation()
}

@Composable
fun AnonymousViewPlayerProfile(
    navController: NavHostController,
    configurationOrientation: Int,
    playerId: Int,
) {
    val viewedPlayer = getPlayerById(playerId)

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.primary)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {
            // Imagem do Perfil
            Image(
                painter = painterResource(id = R.drawable.ic_launcher_background),
                contentDescription = null,
                modifier = Modifier
                    .size(110.dp)
                    .clip(CircleShape)
                    .background(MaterialTheme.colorScheme.primary)
            )

            Button(
                onClick = {
                    navController.navigateUp()
                }, modifier = Modifier
                    .fillMaxWidth()
                    .padding(6.dp)
            ) {
                Text(text = stringResource(R.string.playerProfile_back))
            }

            // Nome do Jogador e Nickname
            Row(
                modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Column {
                    Text(
                        text = viewedPlayer.playerName,
                        style = MaterialTheme.typography.headlineLarge.copy(fontWeight = FontWeight.Bold)
                    )

                    Spacer(modifier = Modifier.height(4.dp))
                    Text(
                        text = viewedPlayer.nickname, style = MaterialTheme.typography.titleSmall
                    )

                    Spacer(modifier = Modifier.height(4.dp))
                }
            }

            Spacer(modifier = Modifier.height(15.dp))

            Text(
                text = viewedPlayer.description, style = MaterialTheme.typography.bodyMedium
            )

            Spacer(modifier = Modifier.height(15.dp))

            if (configurationOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                ) {
                    Spacer(modifier = Modifier.width(10.dp))

                    Column(
                        modifier = Modifier.weight(1f)
                    ) {

                        InfoItem(
                            symbol = "✔",
                            title = stringResource(R.string.playerProfile_correctAnswers),
                            value = viewedPlayer.correctAnswers.toString()
                        )

                        Spacer(modifier = Modifier.height(20.dp))

                        InfoItem(
                            symbol = "❌",
                            title = stringResource(R.string.playerProfile_wrongAnswers),
                            value = viewedPlayer.wrongAnswers.toString()
                        )

                        Spacer(modifier = Modifier.height(20.dp))

                        InfoItem(
                            symbol = "\uD83D\uDCD6",
                            title = stringResource(R.string.playerProfile_totalQuestionsAnswered),
                            value = viewedPlayer.totalQuestionsAnswered.toString()
                        )
                    }

                    Spacer(modifier = Modifier.width(25.dp))

                    Divider(
                        color = Color.Black,
                        thickness = 1.dp,
                        modifier = Modifier
                            .height(170.dp)
                            .width(2.dp)
                    )

                    Spacer(modifier = Modifier.width(35.dp))

                    Column(
                        modifier = Modifier.weight(1f)
                    ) {
                        InfoItem(
                            symbol = "⚙️",
                            title = stringResource(R.string.leaderboard_level),
                            value = viewedPlayer.gameLevel.toString()
                        )

                        Spacer(modifier = Modifier.height(35.dp))

                        InfoItem(
                            symbol = "🎮",
                            title = stringResource(R.string.playerProfile_experience),
                            value = viewedPlayer.gameExperience.toString() + "/" + ((100 * 2.0.pow((viewedPlayer.gameLevel - 1)))).toInt()
                                .toString()
                        )

                        Spacer(modifier = Modifier.height(35.dp))

                        InfoItem(
                            symbol = "🏆",
                            title = "Score",
                            value = viewedPlayer.score.toString()
                        )
                    }
                }
            } else if (configurationOrientation == Configuration.ORIENTATION_PORTRAIT) {
                Column(
                    // Make it scrollable
                    modifier = Modifier
                        .fillMaxWidth()
                        .verticalScroll(rememberScrollState())
                ) {
                    Spacer(modifier = Modifier.height(10.dp))

                    InfoItem(
                        symbol = "✔",
                        title = stringResource(R.string.playerProfile_correctAnswers),
                        value = viewedPlayer.correctAnswers.toString()
                    )

                    Spacer(modifier = Modifier.height(20.dp))

                    InfoItem(
                        symbol = "❌",
                        title = stringResource(R.string.playerProfile_wrongAnswers),
                        value = viewedPlayer.wrongAnswers.toString()
                    )

                    Spacer(modifier = Modifier.height(20.dp))

                    InfoItem(
                        symbol = "\uD83D\uDCD6",
                        title = stringResource(R.string.playerProfile_totalQuestionsAnswered),
                        value = viewedPlayer.totalQuestionsAnswered.toString()
                    )

                    Spacer(modifier = Modifier.height(20.dp))

                    InfoItem(
                        symbol = "⚙️",
                        title = stringResource(R.string.leaderboard_level),
                        value = viewedPlayer.gameLevel.toString()
                    )

                    Spacer(modifier = Modifier.height(20.dp))

                    InfoItem(
                        symbol = "🎮",
                        title = stringResource(R.string.playerProfile_experience),
                        value = viewedPlayer.gameExperience.toString() + "/" + ((100 * 2.0.pow((viewedPlayer.gameLevel - 1)))).toInt()
                            .toString()
                    )
                }
            }
        }
    }
}

@Composable
fun PhoneInputDialog(
    onConfirm: (String, String) -> Unit,
    onDismiss: () -> Unit
) {
    var phoneNumber by remember { mutableStateOf("") }
    var textMessage by remember { mutableStateOf("") }
    val permissionGiven = remember { mutableIntStateOf(0) }
    val ctx = LocalContext.current
    if (
        ActivityCompat.checkSelfPermission(
            ctx,
            Manifest.permission.SEND_SMS
        ) == PackageManager.PERMISSION_GRANTED
    ) {
        permissionGiven.intValue = 1
    }
    val permissionLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) {
                permissionGiven.intValue =1
            }
        }

    LaunchedEffect(key1 = "Permission") {
        permissionLauncher.launch(Manifest.permission.SEND_SMS)
    }
    AlertDialog(
        onDismissRequest = { onDismiss() },
        title = { Text("Enter Phone Number and Text Message") },
        text = {
            Column {
                TextField(
                    value = phoneNumber,
                    onValueChange = { phoneNumber = it },
                    label = { Text("Phone Number") },
                    keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Phone),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = 8.dp)
                )

                TextField(
                    value = textMessage,
                    onValueChange = { textMessage = it },
                    label = { Text("Text Message") },
                    modifier = Modifier.fillMaxWidth()
                )
            }
        },
        confirmButton = {
            Button(
                onClick = {
                    onConfirm(phoneNumber, textMessage)
                    onDismiss()
                }
            ) {
                Text("Confirm")
            }
        },
        dismissButton = {
            Button(
                onClick = { onDismiss() }
            ) {
                Text("Cancel")
            }
        }
    )
}

// Function to send SMS using SmsManager
fun sendSms(phoneNumber: String, textMessage: String) {
    try {
        val smsManager = SmsManager.getDefault()
        smsManager.sendTextMessage(phoneNumber, null, textMessage, null, null)
        Log.d("SMS", "SMS sent")
    } catch (e: Exception) {
        Log.d("SMS", "SMS failed")
        e.printStackTrace()
    }
}
