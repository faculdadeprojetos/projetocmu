package pt.ipp.estg.peddypapper.components.composable

import AnswerHistoryComponent
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import pt.ipp.estg.peddypapper.R
import pt.ipp.estg.peddypapper.components.controllers.UpdateCurrentPlayerClanId
import pt.ipp.estg.peddypapper.components.controllers.calculateNewPlayerValues
import pt.ipp.estg.peddypapper.components.models.PlayerInformation

/**
 * This is the app's navigation graph.
 *
 * Part of it is defined and displayed in [MenuComponent],
 * the composable function that represents the app's menu.
 *
 * @param navController The navigation controller used to navigate to the corresponding screen.
 * @param changeSelectedItem The function used to change the selected item in the navigation drawer.
 * @param disableGesturesEnabled The function used to disable the gestures in the navigation drawer.
 */
@Composable
fun AppNavigationMenu(
    navController: NavHostController,
    changeSelectedItem: (String) -> Unit,
    disableGesturesEnabled: () -> Unit,
    configurationOrientation: Int,
    ctx: Context
) {
    var currentUser by remember { mutableStateOf<PlayerInformation?>(null) }

    // Navigation menu options
    NavHost(
        navController = navController,
        startDestination = "menu"
    ) {
        composable("menu") {
            MenuComponent(navController = navController, changeSelectedItem = changeSelectedItem)
        }

        composable("account") {
            AccountComponent(navController = navController)
        }
        composable("displayClanById/{clanId}") { navBackStackEntry ->
            val clanId = navBackStackEntry.arguments?.getString("clanId")?.toIntOrNull()
            clanId?.let {
                DisplayClanById(clanId = it, navController = navController)
            }
        }
        composable("clan") {
            if (currentUser == null) {
                Toast.makeText(
                    navController.context,
                    stringResource(id = R.string.clan_notLoggedIn),
                    Toast.LENGTH_SHORT
                ).show()

                changeSelectedItem("account")
                navController.navigate("login")

            } else {
                var wasClanIdChanged by remember{ mutableStateOf(false) }
                var newClanId  by remember { mutableIntStateOf(0) }
                CreateClanComponent(navController = navController, currentUser = currentUser!!, changeCurrentUserClanId = {
                    wasClanIdChanged = true
                    newClanId = it

                })
                if(wasClanIdChanged) {
                    UpdateCurrentPlayerClanId(currentUser!!.id, newClanId)
                }
            }
        }

        composable("displayAllClans") {
            if (currentUser == null) {
                Toast.makeText(
                    navController.context,
                    stringResource(id = R.string.clan_notLoggedIn),
                    Toast.LENGTH_SHORT
                ).show()

                changeSelectedItem("account")
                navController.navigate("login")

            } else {
                DisplayAllClans(navController = navController, currentUser = currentUser!!)
            }
        }


        composable("login") {
            if (currentUser != null) {
                RedirectToProfileIfLoggedIn(
                    navController = navController,
                    currentUser = currentUser!!,
                    changeCurrentUser = {
                        currentUser = it
                    }
                )
            } else {
                LoginForm(navController = navController, changeCurrentUser = {
                    currentUser = it
                })
            }
        }

        composable("register") {
            RegisterForm(navController = navController, ctx = ctx)
        }

        composable("editProfile") {
            if (currentUser == null) {
                navController.navigate("login")
            } else {
                EditProfileForm(
                    navController = navController,
                    player = currentUser!!,
                    changeCurrentUser = {
                        currentUser = it
                    },
                    ctx = ctx
                )
            }
        }

        composable("game") {
            if (currentUser == null) {
                Toast.makeText(
                    navController.context,
                    stringResource(id = R.string.game_notLoggedIn),
                    Toast.LENGTH_SHORT
                ).show()

                changeSelectedItem("account")
                navController.navigate("login")
            } else {
                var questionDifficulty by remember { mutableStateOf("") }

                var wasCorrect by remember { mutableStateOf<Boolean?>(null) }

                GameComponent(
                    disableGesturesEnabled = disableGesturesEnabled,
                    questionDifficulty = {
                        questionDifficulty = it
                    },
                    wasAnswerToQuestionCorrect = {
                        wasCorrect = it
                    },
                    playerInformation = currentUser!!,
                    increasePlayerExperience = {
                        currentUser = calculateNewPlayerValues(
                            currentUser = currentUser!!,
                            wasCorrect = true,
                            experienceToGainFromQuestion = it
                        )
                    },
                    ctx = ctx
                )

                if (wasCorrect != null) {
                    UpdatePlayerInformationInDatabase(
                        currentUser = currentUser!!,
                        questionDifficulty = questionDifficulty,
                        wasCorrect = wasCorrect!!,
                    )
                }
            }
        }

        composable("playerProfile") {
            if (currentUser == null) {
                navController.navigate("login")
            } else {
                PlayerProfile(
                    player = currentUser!!,
                    navController = navController,
                    configurationOrientation = configurationOrientation,
                    changeCurrentUser = {
                        currentUser = it
                    }
                )
            }
        }

        composable("playerProfile/{id}") {
            Log.d("PlayerProfile", "PlayerProfile: ${it.arguments?.getString("id")}")

            AnonymousViewPlayerProfile(
                navController = navController,
                configurationOrientation = configurationOrientation,
                playerId = it.arguments?.getString("id")!!.toInt()
            )
        }

        composable("answerHistory") {
            if (currentUser == null) {
                navController.navigate("login")
            } else {
                AnswerHistoryComponent(
                    player = currentUser!!,
                    navController = navController
                )
            }
        }


        composable("leaderboard") {
            val leaderboardList = getAllPlayersForLeaderboard()

            LeaderboardComponent(leaderboardItems = leaderboardList, navController = navController)
        }

        composable("settings") {
            SettingsComponent(ctx)
        }
    }
}

/**
 * This is the app's top bar.
 * It is displayed in [AppScaffold], which is displayed in [AppModalNavigationDrawer].
 *
 * It contains the app's navigation icon, we opted for a hamburger icon,
 * the app's title and a settings icon to navigate to the settings screen.
 *
 * @param scope The coroutine scope used to launch coroutines.
 * @param drawerState The state of the navigation drawer.
 * @param navController The navigation controller used to navigate to the corresponding screen.
 * @param changeSelectedItem The function used to change the selected item in the navigation drawer.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppTopBar(
    scope: CoroutineScope,
    drawerState: DrawerState,
    navController: NavHostController,
    changeSelectedItem: (String) -> Unit
) {
    TopAppBar(
        navigationIcon = {
            IconButton(
                onClick = {
                    scope.launch {
                        drawerState.open()
                    }
                }
            ) {
                Icon(
                    imageVector = Icons.Default.Menu,
                    contentDescription = "Localized description",
                )
            }
        },
        title = {
            Text(
                text = stringResource(R.string.app_name),
                fontFamily = FontFamily.SansSerif,
                fontSize = 30.sp,
                letterSpacing = 0.3.sp,
                modifier = Modifier.padding(start = 50.dp)
            )
        },
        actions = {
            IconButton(
                onClick = {
                    changeSelectedItem("account")
                    navController.navigate("settings")
                },
                modifier = Modifier.padding(start = 50.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.Settings,
                    contentDescription = "Localized description",
                )
            }
        }
    )
}

/**
 * This is the app's scaffold.
 * It is displayed in [AppModalNavigationDrawer].
 *
 * The app's scaffold contains the app's top bar and the app's navigation graph.
 *
 * @param navController The navigation controller used to navigate to the corresponding screen.
 * @param drawerState The state of the navigation drawer.
 * @param changeSelectedItem The function used to change the selected item in the navigation drawer.
 * @param disableGesturesEnabled The function used to disable the gestures in the navigation drawer.
 */
@Composable
fun AppScaffold(
    navController: NavHostController,
    drawerState: DrawerState,
    changeSelectedItem: (String) -> Unit,
    disableGesturesEnabled: () -> Unit,
    configurationOrientation: Int,
    ctx: Context
) {
    val scope = rememberCoroutineScope()

    Scaffold(
        topBar = {
            AppTopBar(
                scope = scope,
                drawerState = drawerState,
                navController = navController,
                changeSelectedItem = changeSelectedItem
            )
        }
    ) {
        Column(
            modifier = Modifier.padding(paddingValues = it)
        ) {
            AppNavigationMenu(
                navController = navController,
                changeSelectedItem = changeSelectedItem,
                disableGesturesEnabled = disableGesturesEnabled,
                configurationOrientation = configurationOrientation,
                ctx = ctx
            )
        }
    }
}

/**
 * This is the app's navigation drawer.
 *
 * It displays the total navigation graph, which is defined in [AppNavigationMenu].
 * as well as the app's top bar, which is defined in [AppTopBar].
 *
 * The navigation drawer is triggered by a swipe gesture from the left edge of the screen,
 * or by clicking on the hamburger icon in the top bar.
 *
 * Clicking on an item in the navigation drawer will navigate to the corresponding screen.
 *
 * The list of items in the navigation drawer is defined according to the components that need to be displayed
 * in the app, in this case, the components are: account, game, leaderboard.
 *
 * @param navController The navigation controller used to navigate to the corresponding screen.
 */
@Composable
fun AppModalNavigationDrawer(
    navController: NavHostController,
    drawerState: DrawerState,
    configurationOrientation: Int,
    ctx: Context
) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination?.route ?: ""

    val drawerItems = listOf(
        "game",
        "leaderboard",
        "account",
        "clan",
    )

    val selectedItem = remember { mutableStateOf("") }

    var isGestureEnabled by remember { mutableStateOf(true) }

    LaunchedEffect(currentDestination) {
        // Disable gestures when navigating to the Game destination
        isGestureEnabled = currentDestination != "game"

        if (!isGestureEnabled) {
            // Disable gestures when navigating to the Game destination
            drawerState.close()
        }
    }

    ModalNavigationDrawer(
        gesturesEnabled = isGestureEnabled,
        drawerState = drawerState,
        drawerContent = {
            ModalDrawerSheet {
                drawerItems.forEach {
                    val itemText = it.replaceFirst(it[0], it[0].uppercaseChar(), true)
                    NavigationDrawerItem(
                        label = {
                            Text(
                                text = itemText,
                                fontFamily = FontFamily.SansSerif,
                                fontSize = 20.sp,
                                letterSpacing = 0.3.sp
                            )
                        },
                        selected = selectedItem.value == it,
                        onClick = {
                            selectedItem.value = it
                            isGestureEnabled = true
                            navController.navigate(it)
                        }
                    )
                }
            }
        },
        content = {
            AppScaffold(
                navController = navController,
                drawerState = drawerState,
                changeSelectedItem = {
                    selectedItem.value = it
                },
                disableGesturesEnabled = {
                    // Change the value of isGestureEnabled to false so compose redraws this composable function
                    // and the gestures are disabled
                    isGestureEnabled = false
                },
                configurationOrientation = configurationOrientation,
                ctx = ctx
            )
        }
    )
}

/**
 * This is a function that displays a ClickableText with a line below it.
 *
 * The ClickableText is used to navigate to the corresponding screen.
 *
 * The line below the ClickableText is used to indicate that the text is clickable.
 *
 * @param text The text to be displayed.
 * @param navController The navigation controller used to navigate to the corresponding screen.
 * @param verticalPadding The vertical padding to be applied to the ClickableText.
 * @param changeSelectedItem The function used to change the selected item in the navigation drawer.
 */
@Composable
fun AppClickableText(
    text: AnnotatedString,
    navController: NavHostController,
    verticalPadding: Dp = 0.dp,
    changeSelectedItem: ((String) -> Unit?)? = null
) {
    ClickableText(
        text = text,
        onClick = {
            if (changeSelectedItem != null) {
                changeSelectedItem(text.text.lowercase())
            }
            navController.navigate(text.text.lowercase())
        },
        modifier = Modifier
            .padding(vertical = verticalPadding)
            .drawBehind {
                val strokeWidthPx = 1.dp.toPx()
                val verticalOffset = size.height - 2.sp.toPx()
                drawLine(
                    color = Color.Black,
                    strokeWidth = strokeWidthPx,
                    start = Offset(0f, verticalOffset),
                    end = Offset(size.width, verticalOffset)
                )
            },
    )
}

@Preview
@Composable
fun PreviewAppModalNavigationDrawer() {
    val navController = rememberNavController()
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Open)

    val configurationOrientation = LocalConfiguration.current.orientation

    val ctx = LocalContext.current

    AppModalNavigationDrawer(
        navController = navController,
        drawerState = drawerState,
        configurationOrientation = configurationOrientation,
        ctx = ctx
    )
}