package pt.ipp.estg.peddypapper.components.models

data class ReplyObject(
    val wasSuccessful: Boolean,
    val message: String
)
