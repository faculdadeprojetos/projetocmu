import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import pt.ipp.estg.peddypapper.R
import pt.ipp.estg.peddypapper.components.data.AppViewModel
import pt.ipp.estg.peddypapper.components.models.Answer
import pt.ipp.estg.peddypapper.components.models.PlayerInformation

@Composable
fun AnswerHistoryComponent(player: PlayerInformation, navController: NavHostController) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Text(
            text = player.playerName,
            style = MaterialTheme.typography.headlineMedium,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(bottom = 8.dp), textAlign = TextAlign.Center
        )


        Text(
            text = stringResource(R.string.playerProfile_answerHistory),
            style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold),
            modifier = Modifier.padding(bottom = 16.dp), textAlign = TextAlign.Center
        )

        // Lista de respostas corretas
        AnswerCategory(stringResource(R.string.playerProfile_correctAnswers), player.id)

        Spacer(modifier = Modifier.height(16.dp))

        // Lista de respostas incorretas
        AnswerCategory(stringResource(R.string.playerProfile_wrongAnswers), player.id)

        /**
         * Button to go back
         */
        Button(
            onClick = {
                navController.navigate("playerProfile")
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Text(text = stringResource(R.string.playerProfile_back))
        }
    }
}


@Composable
fun AnswerCategory(categoryTitle: String, playerId: Int) {
    var isExpanded by remember { mutableStateOf(false) }

    var answers: List<Answer> = mutableListOf()

    when (categoryTitle) {
        stringResource(R.string.playerProfile_correctAnswers) -> {
            answers = getCorrectAnswers(playerId) ?: listOf()
        }

        stringResource(R.string.playerProfile_wrongAnswers) -> {
            answers = getIncorrectAnswers(playerId) ?: listOf()
        }
    }

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                when (categoryTitle) {
                    stringResource(R.string.playerProfile_correctAnswers) -> {
                        MaterialTheme.colorScheme.secondary
                    }

                    stringResource(R.string.playerProfile_wrongAnswers) -> {
                        MaterialTheme.colorScheme.error
                    }

                    else -> {
                        MaterialTheme.colorScheme.primary
                    }
                }
            )
            .clickable { isExpanded = !isExpanded }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 8.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = categoryTitle,
                    style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold),
                    modifier = Modifier.weight(1f)
                )
                Icon(
                    imageVector = if (isExpanded) Icons.Default.ArrowForward else Icons.Default.ArrowBack,
                    contentDescription = null,
                    modifier = Modifier.padding(8.dp)
                )
            }

            if (isExpanded) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp)
                ) {
                    answers.forEach { answer ->
                        Column {
                            Text(
                                text = stringResource(R.string.answer_question) + ": " + answer.question,
                                style = MaterialTheme.typography.bodySmall
                            )
                            Text(
                                text = stringResource(R.string.answer_correctAnswer) + ": " + answer.correctAnswer,
                                style = MaterialTheme.typography.bodySmall
                            )
                            Text(
                                text = stringResource(R.string.answer_answer) + ": " + answer.answer,
                                style = MaterialTheme.typography.bodySmall
                            )
                        }

                        Spacer(modifier = Modifier.width(300.dp).height(20.dp))
                    }
                }
            }
        }
    }
}

/**
 * Get the correct answers from the database
 *
 * @param playerId - Player's id, used to search for the correct answers
 * @return List<Answer>? - List of correct answers, null if there are no correct answers
 */
@Composable
fun getCorrectAnswers(playerId: Int): List<Answer>? {
    val appViewModel: AppViewModel = viewModel()
    val answers = appViewModel.getCorrectAnswers(playerId).observeAsState()

    Log.d("ANSWERS", answers.value.toString())

    val returningAnswers: MutableList<Answer> = mutableListOf()

    answers.value?.let {
        for (answer in it) {
            returningAnswers.add(
                Answer(
                    answer.id,
                    answer.playerId,
                    answer.question,
                    answer.answer,
                    answer.correctAnswer,
                    answer.correct
                )
            )
        }
    } ?: run {
        return null
    }

    return returningAnswers
}

/**
 * Get the incorrect answers from the database
 *
 * @param playerId - Player's id, used to search for the incorrect answers
 * @return List<Answer>? - List of incorrect answers, null if there are no incorrect answers
 */
@Composable
fun getIncorrectAnswers(playerId: Int): List<Answer>? {
    val appViewModel: AppViewModel = viewModel()
    val answers = appViewModel.getIncorrectAnswers(playerId).observeAsState()

    val returningAnswers: MutableList<Answer> = mutableListOf()

    answers.value?.let {
        for (answer in it) {
            returningAnswers.add(
                Answer(
                    answer.id,
                    answer.playerId,
                    answer.question,
                    answer.answer,
                    answer.correctAnswer,
                    answer.correct
                )
            )
        }
    } ?: run {
        return null
    }

    return returningAnswers
}



