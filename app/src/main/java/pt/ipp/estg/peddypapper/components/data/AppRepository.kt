package pt.ipp.estg.peddypapper.components.data

import androidx.lifecycle.LiveData
import pt.ipp.estg.peddypapper.components.models.PlayerInformation
import pt.ipp.estg.peddypapper.components.models.Question
import pt.ipp.estg.peddypapper.components.retrofit.OpenTriviaApi
import pt.ipp.estg.peddypapper.components.retrofit.OpenTriviaModel
import pt.ipp.estg.peddypapper.components.retrofit.OpenTriviaResponse
import retrofit2.Response

class AppRepository(
    private val playerDao: PlayerDao,
    private val questionDao: QuestionDao,
    private val openTriviaApi: OpenTriviaApi
) {

    /**
     * Function to get all player information from firebase - Might not be used
     */
    fun getAllPlayers(): LiveData<List<PlayerInformation>> {
        return playerDao.getAll()
    }

    /**
     * Function to get data from openTriviaAPi
     */

    suspend fun getOpenTriviaQuestions(): Response<OpenTriviaResponse> {
        return this.openTriviaApi.getOpenTriviaQuestions("15")
    }

    /**
     * Function to get all questions from firebase
     */
    fun getQuestions(): LiveData<List<Question>> {
        return questionDao.getAllQuestions()
    }

    /**
     * Function that adds a question into local cache
     */
    suspend fun insertQuestion(question: Question) {
        val counter = questionDao.getQuestionCount()

        if (counter < 15) {
            questionDao.insertQuestion(question)
        }
    }

    /**
     * Function to get player information by id from firebase
     */
    fun getPlayerById(playerId: Int): LiveData<PlayerInformation> {
        return playerDao.getPlayerById(playerId)
    }

    fun getPlayerInformation(username: String, password: String): LiveData<PlayerInformation> {
        return playerDao.getPlayerInformation(username, password)
    }

    /**
     * Function to insert player information into local cache
     */
    suspend fun insertPlayer(playerInformation: PlayerInformation) =
        playerDao.insertPlayer(playerInformation)

    /**
     * Function to update local cache of player information
     */
    suspend fun updatePlayer(playerInformation: PlayerInformation) =
        playerDao.updatePlayer(playerInformation)

    /**
     * Function to delete local cache of player information
     */
    suspend fun deletePlayer(playerInformation: PlayerInformation) =
        playerDao.deletePlayer(playerInformation)
}