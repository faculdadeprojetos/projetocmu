package pt.ipp.estg.peddypapper.components.retrofit

class OpenTriviaModel(
    val difficulty: String,
    val question: String,
    val correct_answer: String,
    val incorrect_answers: List<String>
)

