package pt.ipp.estg.peddypapper.components.composable

import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import pt.ipp.estg.peddypapper.R
import pt.ipp.estg.peddypapper.components.data.AppViewModel
import pt.ipp.estg.peddypapper.components.models.LeaderboardItem

@Composable
fun LeaderboardComponent(
    leaderboardItems: List<LeaderboardItem> = listOf(),
    navController: NavHostController = rememberNavController()
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            text = stringResource(R.string.leaderboard_title),
            style = MaterialTheme.typography.headlineLarge.copy(color = Color.Blue),
            modifier = Modifier.padding(bottom = 16.dp)
        )
        // Table Header
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
        ) {
            Text(
                text = stringResource(R.string.leaderboard_name),
                style = MaterialTheme.typography.titleSmall,
                modifier = Modifier.weight(1f)
            )
            Text(
                text = stringResource(R.string.leaderboard_level),
                style = MaterialTheme.typography.titleSmall,
                modifier = Modifier.weight(1f)
            )
            Text(
                text = stringResource(R.string.leaderboard_score),
                style = MaterialTheme.typography.titleSmall,
                modifier = Modifier.weight(1f)
            )
            Text(
                text = stringResource(R.string.leaderboard_ranking),
                style = MaterialTheme.typography.titleSmall,
                modifier = Modifier.weight(1f)
            )
        }

        Divider(
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp), color = Color.Gray
        )

        if (leaderboardItems.isEmpty()) {
            Text(
                text = stringResource(R.string.leaderboard_empty),
                style = MaterialTheme.typography.bodyLarge,
                modifier = Modifier.padding(top = 16.dp)
            )
        } else {
            LeaderboardTable(leaderboardItems, navController = navController)
        }
    }
}


@Composable
fun LeaderboardTable(leaderboardItems: List<LeaderboardItem>, navController: NavHostController = rememberNavController()) {
    LazyColumn {
        itemsIndexed(leaderboardItems) { index, item ->
            // Send the item and it's position in the list to the composable
            LeaderboardItemRow(item, index, navController = navController)
            Divider(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(1.dp), color = Color.Gray
            )
        }
    }
}

@Composable
fun LeaderboardItemRow(item: LeaderboardItem, index: Int, navController: NavHostController = rememberNavController()) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable(
                onClick = {
                    navController.navigate("playerProfile/${(index + 1)}")
                }
            ),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = item.name,
            style = MaterialTheme.typography.bodySmall,
            modifier = Modifier.weight(1f)
        )
        Text(
            text = item.level.toString(),
            style = MaterialTheme.typography.bodySmall,
            modifier = Modifier.weight(1f)
        )
        Text(
            text = item.score.toString(),
            style = MaterialTheme.typography.bodySmall,
            modifier = Modifier.weight(1f)
        )
        Text(
            text = (index + 1).toString(),
            style = MaterialTheme.typography.bodySmall,
            modifier = Modifier.weight(1f)
        )
    }
}

/**
 * Get all players from the database and convert them to LeaderboardItem
 */
@Composable
fun getAllPlayersForLeaderboard(): MutableList<LeaderboardItem> {
    var leaderboardList = mutableListOf<LeaderboardItem>()
    val appViewModel: AppViewModel = viewModel()
    val listOfPlayers = appViewModel.getAllPlayers().observeAsState()

    listOfPlayers.value?.let { playerList ->
        playerList.forEach { player ->
            val leaderboardItem = LeaderboardItem(
                name = player.playerName,
                level = player.gameLevel,
                score = player.score,
                correctAnswers = player.correctAnswers,
                wrongAnswers = player.wrongAnswers,
            )

            leaderboardList.add(leaderboardItem)
        }
    } ?: run {
        Log.d("AppNavigationMenu", "listOfPlayers is null")
    }

    leaderboardList = leaderboardList.sortedByDescending { it.score }.toMutableList()

    return leaderboardList
}

@Preview
@Composable
fun PreviewLeaderboardComponent() {
    Surface(
        color = MaterialTheme.colorScheme.background
    ) {
        val leaderboardItems = remember { mutableListOf<LeaderboardItem>() }

        val leaderboardItem1 = LeaderboardItem(
            name = "Player 1",
            level = 1,
            score = 100,
            correctAnswers = 10,
            wrongAnswers = 0,
        )

        val leaderboardItem2 = LeaderboardItem(
            name = "Player 2",
            level = 2,
            score = 200,
            correctAnswers = 20,
            wrongAnswers = 0,
        )

        val leaderboardItem3 = LeaderboardItem(
            name = "Player 3",
            level = 3,
            score = 300,
            correctAnswers = 30,
            wrongAnswers = 0,
        )

        leaderboardItems.add(leaderboardItem1)
        leaderboardItems.add(leaderboardItem2)
        leaderboardItems.add(leaderboardItem3)

        LeaderboardComponent(leaderboardItems)
    }
}