package pt.ipp.estg.peddypapper.components.controllers

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.flow.collect
import pt.ipp.estg.peddypapper.components.data.AppViewModel
import pt.ipp.estg.peddypapper.components.models.PlayerInformation

@Composable
fun UpdateCurrentPlayerClanId(playerId: Int, clanId: Int) {
    val ctx = LocalContext.current
    val appViewModel: AppViewModel = viewModel()

    LaunchedEffect(ctx) {
        appViewModel.updatePlayerClan(playerId, clanId).asFlow().collect {

        }
    }


}

@Composable
fun updateClanLevel(clanId: Int): Int {
    var newLevel by remember { mutableIntStateOf(0) }
    val appViewModel: AppViewModel = viewModel()

    LaunchedEffect(Unit) {
        appViewModel.getNewLevelToClan(clanId).asFlow().collect {
            newLevel = it
        }
    }
    return newLevel
}