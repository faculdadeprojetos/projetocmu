package pt.ipp.estg.peddypapper

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.rememberDrawerState
import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.calculateWindowSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.rememberNavController
import pt.ipp.estg.peddypapper.components.composable.AppModalNavigationDrawer
import pt.ipp.estg.peddypapper.components.models.PlayerInformation
import pt.ipp.estg.peddypapper.ui.theme.PeddyPapperTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PeddyPapperTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    // Navigation controller that allows the user to navigate between screens.
                    val navController = rememberNavController()

                    // Drawer state that allows the user to open and close the navigation drawer.
                    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)

                    // Orientation of the device so we know if we are on a phone or tablet
                    // and load player profile accordingly
                    val configuration = LocalConfiguration.current

                    val ctx = LocalContext.current

                    // Component that represents the navigation drawer of the application.
                    AppModalNavigationDrawer(
                        navController = navController,
                        drawerState = drawerState,
                        configurationOrientation = configuration.orientation,
                        ctx = ctx
                    )

                }
            }
        }
    }
}

@Preview
@Composable
fun PreviewApp() {
    PeddyPapperTheme {
        val navController = rememberNavController()
        val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)

        val configuration = LocalConfiguration.current

        val configurationOrientation = configuration.orientation

        val ctx = LocalContext.current

        AppModalNavigationDrawer(
            navController = navController,
            drawerState = drawerState,
            configurationOrientation = configurationOrientation,
            ctx = ctx
        )
    }
}