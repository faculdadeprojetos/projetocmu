// Importe as classes necessárias
import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pt.ipp.estg.peddypapper.R
import pt.ipp.estg.peddypapper.components.controllers.calculateDistanceBetweenUserAndMarker

class DistanceNotificationService : Service() {

    companion object {
        const val EXTRA_USER_LOCATION = "extra_user_location"
        const val EXTRA_MARKER_LOCATIONS = "extra_marker_locations"
    }

    private val binder = LocalBinder()
    private var isServiceRunning = false

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    inner class LocalBinder : Binder() {
        fun getService(): DistanceNotificationService = this@DistanceNotificationService
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (!isServiceRunning) {
            isServiceRunning = true

            // Extrair os dados da Intent
            val userLocation: LatLng? = intent?.getParcelableExtra(EXTRA_USER_LOCATION)
            val markerLocations: List<LatLng>? = intent?.getParcelableArrayListExtra(EXTRA_MARKER_LOCATIONS)

            if (userLocation != null && markerLocations != null) {
                startForegroundService(userLocation, markerLocations)
            } else {

            }
        }
        return START_STICKY
    }

    private fun startForegroundService(userLocation: LatLng, markerLocations: List<LatLng>) {
        val distanceThreshold = 100.0

        GlobalScope.launch(Dispatchers.Main) {
            while (isServiceRunning) {

                val currentUserLocation = userLocation

                // Iterar sobre as localizações dos marcadores
                for (targetLocation in markerLocations) {
                    val distance = calculateDistanceBetweenUserAndMarker(currentUserLocation, targetLocation)

                    // Verificar se o utilizador está dentro da distância desejada
                    if (distance <= distanceThreshold) {
                        // Se o utilizador estiver na distância desejada, exibe a notificação
                        showNotification(
                            applicationContext,
                            "Alerta",
                            "Já se encontram disponiveis as questões para o próximo local"
                        )
                    }
                }

                kotlinx.coroutines.delay(5000) // Espera por 5 segundos (5000 milissegundos)
            }
        }
    }


override fun onDestroy() {
        super.onDestroy()
        isServiceRunning = false
    }
}



/**
 * Função para exibir uma notificação.
 */
fun showNotification(context: Context, title: String, content: String) {
    val channelId = "game_notification_channel"
    val notificationId = 1

    // Criação do canal de notificação (necessário para Android 8.0 em diante)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val channel = NotificationChannel(
            channelId,
            "Game Notifications",
            NotificationManager.IMPORTANCE_DEFAULT
        )
        val notificationManager = context.getSystemService(NotificationManager::class.java)
        notificationManager.createNotificationChannel(channel)
    }

    // Construção da notificação
    val builder = NotificationCompat.Builder(context, channelId)
        .setSmallIcon(R.drawable.notification)
        .setContentTitle(title)
        .setContentText(content)
        .setPriority(NotificationCompat.PRIORITY_DEFAULT)

    // Verifica se a permissão VIBRATE está concedida antes de exibir a notificação
    if (ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.VIBRATE
        ) == PackageManager.PERMISSION_GRANTED
    ) {
        with(NotificationManagerCompat.from(context)) {
            notify(notificationId, builder.build())
        }
    } else {
        // Se a permissão não estiver concedida, você pode solicitar ao usuário que a conceda
        // ou lidar com isso de acordo com os requisitos do seu aplicativo.
        Log.e("Notification", "VIBRATE permission not granted.")
    }
}
