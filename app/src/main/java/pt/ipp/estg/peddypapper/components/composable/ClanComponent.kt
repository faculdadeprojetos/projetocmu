package pt.ipp.estg.peddypapper.components.composable


import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.livedata.observeAsState

import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope

import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource

import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel

import androidx.navigation.NavHostController
import kotlinx.coroutines.launch
import pt.ipp.estg.peddypapper.R
import pt.ipp.estg.peddypapper.components.data.AppViewModel
import pt.ipp.estg.peddypapper.components.models.Clan
import pt.ipp.estg.peddypapper.components.models.PlayerInformation
import pt.ipp.estg.peddypapper.components.models.ReplyObject


@Composable
fun CreateClanComponent(
    navController: NavHostController,
    currentUser: PlayerInformation,
    changeCurrentUserClanId: (clanId: Int) -> Unit
) {
    var clanName by remember { mutableStateOf(TextFieldValue()) }
    var clanTag by remember { mutableStateOf(TextFieldValue()) }
    var openAlertDialog by remember { mutableStateOf(false) }
    var hasRegisterBeenClicked by remember { mutableStateOf(false) }


    // This is the scope for the coroutine.
    val scope = rememberCoroutineScope()

    val appViewModel: AppViewModel = viewModel()
    var newLevel by remember { mutableIntStateOf(0) }

    val ctx: Context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Text(
            text = "Criar Clã",
            style = MaterialTheme.typography.headlineMedium,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(bottom = 8.dp)
        )


        OutlinedTextField(
            value = clanName,
            onValueChange = { clanName = it },
            label = { Text(text = "Nome do Clã") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 8.dp)
        )


        OutlinedTextField(
            value = clanTag,
            onValueChange = { clanTag = it },
            label = { Text(text = "Tag do clan") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp)
        )
        val clan = Clan(
            id = 0,
            name = clanName.text,
            tag = clanTag.text,
            level = currentUser.gameLevel,
            rank = "new",
        )

        if (hasRegisterBeenClicked) {
            if (clanName.text.isBlank() || clanName.text.contains(Regex("[^A-Za-z0-9 ]"))) {
                Text(text = "Nome não pode ser nulo ou conter careteres especiais!!")
            } else if (clanTag.text.isBlank() || clanTag.text.length > 5) {
                Text(text = "Tag não pode ser nulo ou ter tamanho maior que 5")
            } else if (currentUser.gameLevel < 1) {
                Text(text = "Tens de ter pelo menos lvl 5 para poder criar um clã")
            } else {
                Log.d("clan information", "Entered hasRegisterBeenClicked")
                LaunchedEffect(Unit) {
                    Log.d("clan information", "Entered LaunchedEffect")
                    appViewModel.insertClan(clan, currentUser, changeCurrentUserClanId).asFlow()
                        .collect { result: ReplyObject ->
                            Log.d("clan information", "Entered collect")
                            Log.d("clan information", result.toString())

                            if (result.wasSuccessful) {

                                scope.launch {
                                    appViewModel.getNewLevelToClan(clan.id).asFlow().collect {
                                        newLevel = it
                                        appViewModel.updateClanLevel(clan.id, newLevel)
                                    }


                                }
                                // Display a Toast indicating success.
                                Toast.makeText(
                                    ctx,
                                    result.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                                // Change the screen to the display all clans screen.
                                navController.navigate("displayAllClans")
                            } else {
                                scope.launch {
                                    // Display a Toast indicating failure.
                                    Toast.makeText(
                                        ctx,
                                        result.message,
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        }
                }
            }

        }


        Button(
            onClick = {
                hasRegisterBeenClicked = true;
                openAlertDialog = true;
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Text(text = "Criar Clã")
        }
        Button(
            onClick = { navController.navigate("displayAllClans") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Text("Ver lista de clans")
        }
        Text(
            text = "Apenas jogadores de nível 5 ou superior podem criar um clã.",
            color = Color.Red,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp)
        )
    }
}

@Composable
fun DisplayAllClans(navController: NavHostController, currentUser: PlayerInformation) {
    var searchQuery by remember { mutableStateOf("") }

    val appViewModel: AppViewModel = viewModel()
    val clansState = appViewModel.getAllClans().observeAsState()


    // Create a separate mutable state to hold the list of clans
    var clanList by remember { mutableStateOf<List<Clan>>(emptyList()) }

    // Update clanList whenever clansState.value changes
    clanList = clansState.value ?: emptyList()

    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        item {
            Text(
                text = stringResource(R.string.clanList),
                color = Color.Blue,
                style = MaterialTheme.typography.headlineSmall,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(vertical = 8.dp, horizontal = 50.dp)
            )
            SearchBar(searchQuery = searchQuery, onSearchQueryChange = { searchQuery = it })
            Divider(modifier = Modifier.padding(horizontal = 16.dp))
        }

        items(clanList.filter { it.name.contains(searchQuery, ignoreCase = true) }) { clan ->
            ClanItem(clan = clan, currentUser = currentUser, navController) { updatedClan ->
                // Update the clanList with the new values
                clanList = clanList.map { if (it.id == updatedClan.id) updatedClan else it }
            }
            Divider(modifier = Modifier.padding(horizontal = 16.dp))
        }

        item {
            Button(
                onClick = {
                    navController.navigate("clan")
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            ) {
                Text(text = stringResource(id = R.string.create_clan))
            }

            Button(
                onClick = {
                    navController.navigate("game")
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            ) {
                Text(text = stringResource(id = R.string.go_game))
            }
        }
    }
}

@Composable
fun SearchBar(searchQuery: String, onSearchQueryChange: (String) -> Unit) {
    OutlinedTextField(
        value = searchQuery,
        onValueChange = { onSearchQueryChange(it) },
        label = { Text("Pesquisar Clans") },
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
    )
}

@Composable
fun ClanItem(
    clan: Clan,
    currentUser: PlayerInformation,
    navController: NavHostController,
    updateClanItem: (clan: Clan) -> Unit
) {
    var isExpanded by remember { mutableStateOf(false) }
    val appViewModel: AppViewModel = viewModel()
    val ctx = LocalContext.current
    var showMembersList by remember { mutableStateOf(false) }

    var newLevel by remember { mutableStateOf(0) }

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable {
                isExpanded = !isExpanded
            }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Text(
                text = "Nome: ${clan.name}",
                fontWeight = FontWeight.Bold,
                color = Color.Black
            )
            Text(
                text = "Tag: ${clan.tag}",
                fontWeight = FontWeight.Bold,
                color = Color.Black
            )
            Text(
                text = "Rank: ${clan.rank}",
                fontWeight = FontWeight.Bold,
                color = Color.Black
            )
            Text(
                text = "Level: ${clan.level}",
                fontWeight = FontWeight.Bold,
                color = Color.Black
            )

            if (isExpanded) {
                Spacer(modifier = Modifier.height(8.dp))

                Button(
                    onClick = {

                        appViewModel.viewModelScope.launch {
                            appViewModel.insertPlayerIntoClan(currentUser, clan.id).asFlow()
                                .collect { result: ReplyObject ->
                                    if (result.wasSuccessful) {
                                        Toast.makeText(
                                            ctx,
                                            "Entrou no clã com sucesso!",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        appViewModel.getNewLevelToClan(clan.id).asFlow().collect {
                                            newLevel = it
                                            updateClanItem(clan.copy(level = newLevel))
                                        }
                                    } else {
                                        Toast.makeText(
                                            ctx,
                                            "Erro ao tentar entrar no clã: ${result.message}",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                        }
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp)
                ) {
                    Text(text = stringResource(R.string.join_clan))
                }
                Spacer(modifier = Modifier.height(8.dp))

                Button(
                    onClick = {
                        navController.navigate("displayClanById/${clan.id}")
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp)
                ) {
                    Text(text = stringResource(R.string.check_clan_profile))
                }
            }
        }
    }
}

@Composable
fun DisplayClanById(clanId: Int, navController: NavHostController) {
    val appViewModel: AppViewModel = viewModel()
    val clan by appViewModel.getClanById(clanId).observeAsState()

    var showMembersList by remember { mutableStateOf(false) }

    val clanName = clan?.name.toString()
    val clanTag = clan?.tag.toString()
    val clanLevel = clan?.level
    val clanRank = clan?.rank.toString()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        if (!showMembersList) {

            InfoItem(
                symbol = "⚙️",
                title = "Nome do Clã",
                value = clanName
            )

            Spacer(modifier = Modifier.height(10.dp))

            InfoItem(
                symbol = "⚙️",
                title = "Tag do Clã",
                value = clanTag
            )

            Spacer(modifier = Modifier.height(10.dp))

            InfoItem(
                symbol = "⚙️",
                title = "Level do Clã",
                value = clanLevel.toString()
            )

            Spacer(modifier = Modifier.height(10.dp))

            InfoItem(
                symbol = "⚙️",
                title = "Rank do Clã",
                value = clanRank
            )

            Button(
                onClick = {
                    showMembersList = true
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            ) {
                Text(text = stringResource(R.string.clan_members))
            }
        } else {
            val clanMembers = appViewModel.getClanMembers(clanId).observeAsState().value
            ClanMembersList(clanMembers) {
                showMembersList = false
            }
        }
    }
}

@Composable
fun ClanMembersList(clanMembers: List<PlayerInformation>?, onBack: () -> Unit = {}) {
    val sortedMembers = clanMembers?.sortedByDescending { it.gameLevel } ?: emptyList()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(2.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f),
            contentPadding = PaddingValues(2.dp)
        ) {
            items(sortedMembers) { member ->
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(2.dp)
                ) {

                    InfoItem(
                        symbol = "⚙️",
                        title = "Nome do Membro ${member.playerName} - level ${member.gameLevel}",
                        value = ""
                    )
                    Spacer(modifier = Modifier.height(4.dp))

                }
            }
        }


        Button(
            onClick = onBack,
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ) {
            Text(text = stringResource(R.string.back_clans))
        }
    }
}


