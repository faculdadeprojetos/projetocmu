package pt.ipp.estg.peddypapper.components.controllers

import android.location.Location
import androidx.compose.runtime.mutableFloatStateOf
import com.google.android.gms.maps.model.LatLng
import pt.ipp.estg.peddypapper.components.composable.EASY_QUESTION_EXP
import pt.ipp.estg.peddypapper.components.composable.HARD_QUESTION_EXP
import pt.ipp.estg.peddypapper.components.composable.MEDIUM_QUESTION_EXP
import pt.ipp.estg.peddypapper.components.models.PlayerInformation
import kotlin.math.pow

/**
 * Function that calculates the experience that the user will gain from answering a question correctly.
 */
fun calculateExperienceToGainFromQuestion(questionDifficulty: String): Int {
    return when (questionDifficulty) {
        "easy" -> EASY_QUESTION_EXP
        "medium" -> MEDIUM_QUESTION_EXP
        "hard" -> HARD_QUESTION_EXP
        else -> 0
    }
}

/**
 * Function that calculates the new values of the player after answering a question.
 */
fun calculateNewPlayerValues(
    currentUser: PlayerInformation,
    wasCorrect: Boolean,
    experienceToGainFromQuestion: Int
): PlayerInformation {
    val newPlayer: PlayerInformation?

    if (wasCorrect) {
        val currentLevelUpThreshold = (100 * (2.0.pow(
            currentUser.gameLevel.minus(
                1
            )
        ))).toInt()

        val tempNewExperience =
            currentUser.gameExperience.plus(experienceToGainFromQuestion)
        var newLevel = currentUser.gameLevel

        val finalNewExperience: Int

        if (tempNewExperience >= currentLevelUpThreshold) {
            newLevel += 1
            finalNewExperience = tempNewExperience - currentLevelUpThreshold
        } else {
            finalNewExperience = tempNewExperience
        }

        newPlayer = currentUser.copy(
            correctAnswers = currentUser.correctAnswers.plus(1),
            totalQuestionsAnswered = currentUser.totalQuestionsAnswered.plus(1),
            score = currentUser.score.plus(experienceToGainFromQuestion),
            gameLevel = newLevel,
            gameExperience = finalNewExperience
        )
    } else {
        newPlayer = currentUser.copy(
            wrongAnswers = currentUser.wrongAnswers.plus(1),
            totalQuestionsAnswered = currentUser.totalQuestionsAnswered.plus(1)
        )
    }

    return newPlayer
}

/**
 * Function that calculates the distance between the user and a marker.
 *
 * @param currentLocation Location of the user.
 * @param marker Location of the marker.
 */
fun calculateDistanceBetweenUserAndMarker(
    currentLocation: LatLng,
    marker: LatLng
): Float {
    val results = FloatArray(1)
    Location.distanceBetween(
        currentLocation.latitude, currentLocation.longitude,
        marker.latitude, marker.longitude,
        results
    )
    return results[0]
}