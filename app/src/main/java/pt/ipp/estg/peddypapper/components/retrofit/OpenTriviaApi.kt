package pt.ipp.estg.peddypapper.components.retrofit

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenTriviaApi {
    @GET("api.php")
    suspend fun getOpenTriviaQuestions(
        @Query("amount") amount: String
    ): Response<OpenTriviaResponse>
}