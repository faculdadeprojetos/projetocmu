package pt.ipp.estg.peddypapper.components.models


data class Clan (
    var id: Int = 0,
    val name: String,
    val tag: String,
    val level: Int = 0,
    val rank: String
) {
    constructor() : this(0, "", "", 0, "")
}