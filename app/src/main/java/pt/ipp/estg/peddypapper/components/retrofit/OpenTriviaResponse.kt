package pt.ipp.estg.peddypapper.components.retrofit

data class OpenTriviaResponse (
    val response_code: Int,
    val results: List<OpenTriviaModel>
)