package pt.ipp.estg.peddypapper.components.data

import android.app.Application
import android.util.Log
import androidx.compose.runtime.mutableIntStateOf
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.AggregateSource
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import pt.ipp.estg.peddypapper.components.models.Answer
import pt.ipp.estg.peddypapper.components.models.Clan
import pt.ipp.estg.peddypapper.components.models.PlayerInformation
import pt.ipp.estg.peddypapper.components.models.Question
import pt.ipp.estg.peddypapper.components.models.ReplyObject
import pt.ipp.estg.peddypapper.components.retrofit.OpenTriviaApi
import pt.ipp.estg.peddypapper.components.retrofit.RetrofitHelper

class AppViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: AppRepository
    private val database: FirebaseFirestore

    init {
        val roomDb = AppDatabase.getDatabase(application)
        database = Firebase.firestore

        val openTriviaEndpoint = "https://opentdb.com/"
        val openTriviaApi =
            RetrofitHelper.getInstance(openTriviaEndpoint).create(OpenTriviaApi::class.java)
        repository = AppRepository(roomDb.playerDao(), roomDb.questionDao(), openTriviaApi)

    }

    /**
     * Function to get Data from openTriviaApi and insert it into the local database cache
     */
    private fun getQuestionsFromOpenTriviaApi() {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.getOpenTriviaQuestions()
            if (response.isSuccessful) {
                val content = response.body()
                content?.results?.forEach {
                    val incorrectAnswers = it.incorrect_answers.joinToString(separator = ",")
                    insertQuestion(
                        Question(
                            0,
                            it.question,
                            it.correct_answer,
                            incorrectAnswers,
                            it.difficulty
                        )
                    )
                }
            }
        }
    }

    /**
     * Function to get a player from the local database by username and password
     */
    fun getPlayerInformation(username: String, password: String): LiveData<PlayerInformation?> {
        val player = MutableLiveData<PlayerInformation?>()

        Log.d("getPlayerInformation", "getPlayerInformation")

        database.collection("players").whereEqualTo("playerName", username).get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    for (document in task.result) {
                        val databasePlayer = document.toObject(PlayerInformation::class.java)
                        Log.d("Player password:", databasePlayer.password)
                        Log.d("Player password:", password)
                        if (databasePlayer.password == password) {
                            player.postValue(databasePlayer)
                            return@addOnCompleteListener
                        }
                    }

                    player.postValue(null)
                } else {
                    println("Error getting documents: ${task.exception}")
                }
            }

        return player
    }

    /**
     * Function to get all players from the local database
     */
    fun getAllPlayers(): LiveData<List<PlayerInformation>> {
        val players = MutableLiveData<List<PlayerInformation>>()

        Log.d("getAllPlayers", "getAllPlayers")

        database.collection("players").get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val playerList = mutableListOf<PlayerInformation>()
                for (document in task.result) {
                    val player = document.toObject(PlayerInformation::class.java)
                    playerList.add(player)
                }

                players.postValue(playerList)
            } else {
                println("Error getting documents: ${task.exception}")
            }
        }

        return players
    }

    fun getAllClans(): LiveData<List<Clan>> {
        val clans = MutableLiveData<List<Clan>>()

        Log.d("getAllClans", "getAllClans")

        database.collection("clans").get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val clanList = mutableListOf<Clan>()
                for (document in task.result) {
                    val clan = document.toObject(Clan::class.java)
                    clanList.add(clan)
                }

                clans.postValue(clanList)
            } else {
                println("Error getting clans: ${task.exception}")
            }
        }

        return clans
    }


    /**
     *  Function to get 15 questions from the API and return the list of questions
     */
    fun getAllQuestions(): LiveData<List<Question>> {
        viewModelScope.launch(Dispatchers.IO) {
            getQuestionsFromOpenTriviaApi()
        }

        return repository.getQuestions()
    }

    /**
     * Function to get the correct answers of a player from firebase
     */
    fun getCorrectAnswers(playerId: Int): LiveData<List<Answer>> {
        val correctAnswers = MutableLiveData<List<Answer>>()

        database.collection("answers").whereEqualTo("playerId", playerId).get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val answerList = mutableListOf<Answer>()

                    for (document in task.result) {
                        val answer = document.toObject(Answer::class.java)
                        if (answer.correct) {
                            answerList.add(
                                Answer(
                                    answer.id,
                                    answer.playerId,
                                    answer.question,
                                    answer.answer,
                                    answer.correctAnswer,
                                    answer.correct
                                )
                            )
                        }
                    }

                    correctAnswers.postValue(answerList)
                } else {
                    println("Error getting documents: ${task.exception}")
                }
            }

        return correctAnswers
    }

    /**
     * Function to insert an answer into firebase
     */
    fun insertAnswer(answer: Answer): LiveData<ReplyObject> {
        val isSuccessful = MutableLiveData<ReplyObject>()

        viewModelScope.launch(Dispatchers.IO) {
            // Verificar se já existe uma resposta com o mesmo id
            // Se já existir, retornar falso
            // Caso contrário, criar uma nova resposta na base de dados
            database.collection("answers").whereEqualTo("id", answer.id).get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && !task.result.isEmpty) {
                        isSuccessful.postValue(
                            ReplyObject(
                                false,
                                "Answer with that id already exists"
                            )
                        )
                    } else {
                        val countQuery = database.collection("answers").count()

                        countQuery.get(AggregateSource.SERVER).addOnCompleteListener { countTask ->

                            if (countTask.isSuccessful) {
                                val snapshot = countTask.result

                                answer.id = snapshot.count.toInt() + 1

                                val documentReference =
                                    database.collection("answers")
                                        .document(answer.id.toString())

                                documentReference.set(answer)
                                    .addOnSuccessListener {
                                        isSuccessful.postValue(
                                            ReplyObject(
                                                true,
                                                "Answer added successfully"
                                            )
                                        )
                                    }
                                    .addOnFailureListener {
                                        isSuccessful.postValue(
                                            ReplyObject(
                                                false,
                                                "Failed to add answer"
                                            )
                                        )
                                    }
                            } else {
                                isSuccessful.postValue(
                                    ReplyObject(
                                        false,
                                        "Failed to get answer count"
                                    )
                                )
                            }
                        }
                    }
                }
        }

        return isSuccessful
    }

    /**
     * Function to get the incorrect answers of a player from firebase
     */
    fun getIncorrectAnswers(playerId: Int): LiveData<List<Answer>> {
        val incorrectAnswers = MutableLiveData<List<Answer>>()

        database.collection("answers").whereEqualTo("playerId", playerId).get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val answerList = mutableListOf<Answer>()

                    for (document in task.result) {
                        val answer = document.toObject(Answer::class.java)
                        if (!answer.correct) {
                            answerList.add(
                                Answer(
                                    answer.id,
                                    answer.playerId,
                                    answer.question,
                                    answer.answer,
                                    answer.correctAnswer,
                                    answer.correct
                                )
                            )
                        }
                    }

                    incorrectAnswers.postValue(answerList)
                } else {
                    println("Error getting documents: ${task.exception}")
                }
            }

        return incorrectAnswers
    }

    /**
     * Function that receives a Question and add it into local database
     */
    private fun insertQuestion(question: Question) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insertQuestion(question)
        }
    }

    /**
     * Function used to insert a player into firebase and the local database
     */
    fun insertPlayer(playerInformation: PlayerInformation): LiveData<ReplyObject> {
        val isSuccessful = MutableLiveData<ReplyObject>()

        viewModelScope.launch(Dispatchers.IO) {
            // Verify if there's a player with the same email already registered
            // If there's already a player with that email, return false
            // Otherwise, create a new player in the database
            database.collection("players").whereEqualTo("email", playerInformation.email).get()
                .addOnCompleteListener { task ->
                    Log.d("Task result is successful:", "${task.isSuccessful}")
                    Log.d("Task result is empty:", "${task.result.isEmpty}")
                    Log.d("email:", playerInformation.email)
                    if (task.isSuccessful && !task.result.isEmpty) {
                        isSuccessful.postValue(
                            ReplyObject(
                                false,
                                "Player with that email already exists"
                            )
                        )

                        return@addOnCompleteListener
                    } else if (task.isSuccessful && task.result.isEmpty) {
                        val countQuery = database.collection("players").count()

                        countQuery.get(AggregateSource.SERVER).addOnCompleteListener { countTask ->
                            if (countTask.isSuccessful) {
                                val snapshot = countTask.result

                                playerInformation.id = snapshot.count.toInt() + 1

                                val documentReference =
                                    database.collection("players")
                                        .document(playerInformation.id.toString())

                                documentReference.set(playerInformation)
                                    .addOnSuccessListener {
                                        isSuccessful.postValue(
                                            ReplyObject(
                                                true,
                                                "Player added successfully"
                                            )
                                        )

                                        return@addOnSuccessListener
                                    }
                                    .addOnFailureListener {
                                        isSuccessful.postValue(
                                            ReplyObject(
                                                false,
                                                "Failed to add player"
                                            )
                                        )

                                        return@addOnFailureListener
                                    }

                            } else {
                                Log.d("Error getting documents:", task.exception.toString())
                                isSuccessful.postValue(
                                    ReplyObject(
                                        false,
                                        "Failed to get player count"
                                    )
                                )
                            }
                        }

                        return@addOnCompleteListener
                    }
                }
        }

        return isSuccessful
    }

    // Função para inserir um clã no Firebase
    fun insertClan(
        clan: Clan,
        player: PlayerInformation,
        clanIdCallback: (clanId: Int) -> Unit
    ): LiveData<ReplyObject> {
        val isSuccessful = MutableLiveData<ReplyObject>()

        viewModelScope.launch(Dispatchers.IO) {
            // Verificar se já existe um clã com o mesmo nome ou tag
            // Se já existir, retornar falso
            // Caso contrário, criar um novo clã na base de dados
            database.collection("clans").whereEqualTo("name", clan.name).get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && !task.result.isEmpty) {
                        isSuccessful.postValue(
                            ReplyObject(
                                false,
                                "Clan with that name already exists or u already have a clan of ur own"
                            )
                        )
                    } else  {
                        if(player.clanId != 0){
                            ReplyObject(
                                wasSuccessful = false,
                                "clan cant be created because user already has a clan"

                            )
                            return@addOnCompleteListener
                        }

                        val countQuery = database.collection("clans").count()

                        countQuery.get(AggregateSource.SERVER).addOnCompleteListener { countTask ->

                            if (countTask.isSuccessful) {
                                val snapshot = countTask.result

                                clan.id = snapshot.count.toInt() + 1

                                val documentReference =
                                    database.collection("clans")
                                        .document(clan.id.toString())

                                documentReference.set(clan)
                                    .addOnSuccessListener {
                                        isSuccessful.postValue(
                                            ReplyObject(
                                                true,
                                                "Clan added successfully"
                                            )
                                        )
                                    }
                                    .addOnFailureListener {
                                        isSuccessful.postValue(
                                            ReplyObject(
                                                false,
                                                "Failed to add clan"
                                            )
                                        )
                                    }

                                clanIdCallback.invoke(clan.id)
                            } else {
                                isSuccessful.postValue(
                                    ReplyObject(
                                        false,
                                        "Failed to get clan count"
                                    )
                                )
                            }
                        }
                    }
                }
        }

        return isSuccessful
    }
    fun updatePlayerClan(playerId: Int, clanId: Int): LiveData<ReplyObject> {
        val isSuccessful = MutableLiveData<ReplyObject>()

        viewModelScope.launch(Dispatchers.IO) {
            // Update the player's clanId
            database.collection("players").document(playerId.toString()).get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val document = task.result
                        if (document.exists()) {
                            val player = document.toObject(PlayerInformation::class.java)
                            if (player != null) {
                                // Update the player's clanId
                                player.clanId = clanId

                                // Save the updated player information
                                val documentReference = database.collection("players")
                                    .document(playerId.toString())

                                documentReference
                                    .set(player)
                                    .addOnSuccessListener {
                                        isSuccessful.postValue(
                                            ReplyObject(
                                                true,
                                                "Player clanId updated successfully"
                                            )
                                        )
                                    }
                                    .addOnFailureListener {
                                        isSuccessful.postValue(
                                            ReplyObject(
                                                false,
                                                "Failed to update player clanId"
                                            )
                                        )
                                    }
                            }
                        } else {
                            isSuccessful.postValue(
                                ReplyObject(
                                    false,
                                    "Player not found with ID: $playerId"
                                )
                            )
                        }
                    } else {
                        println("Error getting player document: ${task.exception}")
                        isSuccessful.postValue(
                            ReplyObject(
                                false,
                                "Failed to get player information"
                            )
                        )
                    }
                }
        }

        return isSuccessful
    }
    fun insertPlayerIntoClan(player: PlayerInformation, clanId: Int): LiveData<ReplyObject> {
        val isSuccessful = MutableLiveData<ReplyObject>()

        viewModelScope.launch(Dispatchers.IO) {
            // Verificar se o jogador já tem um clanId diferente de 0 que é o default, caso tenha já tem clan e nao pode entrar noutro
            if (player.clanId != 0) {
                isSuccessful.postValue(
                    ReplyObject(
                        false,
                        "Player is already associated with a clan"
                    )
                )
            } else {
                // Associar o jogador ao clã
                player.clanId = clanId

                // Atualizar as informações do jogador na base de dados
                val documentReference = database.collection("players")
                    .document(player.id.toString())
                documentReference.set(player)
                    .addOnSuccessListener {
                        isSuccessful.postValue(
                            ReplyObject(
                                true,
                                "Player associated with clan successfully"
                            )
                        )
                    }
                    .addOnFailureListener {
                        isSuccessful.postValue(
                            ReplyObject(
                                false,
                                "Failed to associate player with clan"
                            )
                        )
                    }
            }
        }

        return isSuccessful
    }

    fun updateClanLevel(clanId: Int, newLevel: Int): LiveData<ReplyObject> {
        val isSuccessful = MutableLiveData<ReplyObject>()

        viewModelScope.launch(Dispatchers.IO) {


                        // Atualizar o nível e rank do clã na base de dados
                        database.collection("clans")
                            .document(clanId.toString())
                            .update(
                                mapOf(
                                    "level" to newLevel,
                                    "rank" to calculateRank(newLevel)
                                )
                            )
                            .addOnSuccessListener {
                                isSuccessful.postValue(
                                    ReplyObject(
                                        true,
                                        "Clan level and rank updated successfully"
                                    )
                                )
                            }
                            .addOnFailureListener {
                                isSuccessful.postValue(
                                    ReplyObject(
                                        false,
                                        "Failed to update clan level and rank"
                                    )
                                )
                            }
                    }



        return isSuccessful
    }


    fun getClanById(clanId: Int): LiveData<Clan> {
        val clan = MutableLiveData<Clan>()

        viewModelScope.launch(Dispatchers.IO) {
            database.collection("clans").document(clanId.toString()).get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val document = task.result
                        val clanData = document?.toObject(Clan::class.java)

                        clanData?.let {
                            clan.postValue(it)
                        }
                    } else {
                        Log.d("Error getting documents:", task.exception.toString())
                    }
                }
        }

        return clan
    }

    //create a function that returns all members from a given clanid
    fun getClanMembers(clanId: Int): LiveData<List<PlayerInformation>> {
        val clanMembers = MutableLiveData<List<PlayerInformation>>()

        viewModelScope.launch(Dispatchers.IO) {
            database.collection("players").whereEqualTo("clanId", clanId).get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val playerList = mutableListOf<PlayerInformation>()
                        for (document in task.result) {
                            val player = document.toObject(PlayerInformation::class.java)
                            playerList.add(player)
                        }

                        clanMembers.postValue(playerList)
                    } else {
                        println("Error getting documents: ${task.exception}")
                    }
                }
        }

        return clanMembers
    }
    fun getNewLevelToClan(clanId: Int): LiveData<Int> {
        val averageLevel = MutableLiveData<Int>()

        // Obter a lista de jogadores associados ao clã
        database.collection("players")
            .whereEqualTo("clanId", clanId)
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val playerList = mutableListOf<PlayerInformation>()
                    for (document in task.result) {
                        val player = document.toObject(PlayerInformation::class.java)
                        playerList.add(player)
                    }

                    // Calcular a média dos níveis dos jogadores
                    val totalLevels = playerList.sumOf { it.gameLevel }
                    val newAverageLevel =
                        if (playerList.isNotEmpty()) {
                            totalLevels / playerList.size
                        } else {
                            0
                        }

                    averageLevel.postValue(newAverageLevel)
                }
            }

        return averageLevel
    }

    /**
     * support function to calculateRank based on average level of players
     */
    private fun calculateRank(averageLevel: Int): String {
        return when (averageLevel) {
            0 -> "New"
            1 -> "Uff Noobs"
            2 -> "Average Guys"
            3 -> "Half Way There"
            4 -> "Better Than Some"
            5 -> "Good Guys"
            6 -> "That's How You Do It"
            7 -> "Awesome Players"
            8 -> "Legends"
            9 -> "Phoenix"
            else -> "Phoenix"
        }
    }

    /**
     * Function used to update a player in the local database
     */
    fun updatePlayer(playerInformation: PlayerInformation): LiveData<ReplyObject> {
        val isSuccessful = MutableLiveData<ReplyObject>()

        viewModelScope.launch(Dispatchers.IO) {
            repository.updatePlayer(playerInformation)

            // Verify if there's a player with the same email already registered
            // and if there is, check if they have the same id
            // If there's already a player with that email and different id, return false
            // Otherwise, update the player in the database with the new information
            database.collection("players").whereEqualTo("email", playerInformation.email).get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (document in task.result) {
                            val player = document.toObject(PlayerInformation::class.java)
                            if (player.id != playerInformation.id) {
                                isSuccessful.postValue(
                                    ReplyObject(
                                        false,
                                        "Player with that email already exists"
                                    )
                                )
                                return@addOnCompleteListener
                            }
                        }

                        val documentReference =
                            database.collection("players")
                                .document(playerInformation.id.toString())

                        documentReference
                            .set(playerInformation)
                            .addOnSuccessListener {
                                isSuccessful.postValue(
                                    ReplyObject(
                                        true,
                                        "Player updated successfully"
                                    )
                                )

                                return@addOnSuccessListener
                            }
                            .addOnFailureListener {
                                isSuccessful.postValue(
                                    ReplyObject(
                                        false,
                                        "Failed to update player"
                                    )
                                )

                                return@addOnFailureListener
                            }
                    } else {
                        println("Error getting documents: ${task.exception}")
                        isSuccessful.postValue(
                            ReplyObject(
                                false,
                                "Failed to get player information"
                            )
                        )

                        return@addOnCompleteListener
                    }
                }
        }

        return isSuccessful
    }

    /**
     * Function used to get a player by id from firebase
     */
    fun getPlayerById(playerId: Int): LiveData<PlayerInformation> {
        val player = MutableLiveData<PlayerInformation>()

        viewModelScope.launch(Dispatchers.IO) {
            repository.getPlayerById(playerId)

            database.collection("players").document(playerId.toString()).get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val document = task.result
                        val playerData = document?.toObject(PlayerInformation::class.java)

                        playerData?.let {
                            player.postValue(it)
                        }
                    } else {
                        Log.d("Error getting documents:", task.exception.toString())
                    }
                }
        }

        return player
    }

    /**
     * Function used to delete a player from the local database
     */
    private fun deletePlayer(playerInformation: PlayerInformation): LiveData<ReplyObject> {
        val isSuccessful = MutableLiveData<ReplyObject>()

        viewModelScope.launch(Dispatchers.IO) {
            repository.deletePlayer(playerInformation)

            database.collection("players").document(playerInformation.id.toString()).delete()
                .addOnSuccessListener {
                    isSuccessful.postValue(ReplyObject(true, "Player deleted successfully"))
                }
                .addOnFailureListener {
                    isSuccessful.postValue(ReplyObject(false, "Failed to delete player"))
                }
        }

        return isSuccessful
    }
}