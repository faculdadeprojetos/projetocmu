package pt.ipp.estg.peddypapper.components.composable

import DistanceNotificationService
import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.hardware.TriggerEvent
import android.hardware.TriggerEventListener
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.Priority
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.Circle
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MapProperties
import com.google.maps.android.compose.MapUiSettings
import com.google.maps.android.compose.MarkerInfoWindow
import com.google.maps.android.compose.rememberCameraPositionState
import com.google.maps.android.compose.rememberMarkerState
import pt.ipp.estg.peddypapper.R
import pt.ipp.estg.peddypapper.components.controllers.calculateDistanceBetweenUserAndMarker
import pt.ipp.estg.peddypapper.components.controllers.calculateExperienceToGainFromQuestion
import pt.ipp.estg.peddypapper.components.controllers.calculateNewPlayerValues
import pt.ipp.estg.peddypapper.components.data.AppViewModel
import pt.ipp.estg.peddypapper.components.models.PlayerInformation
import pt.ipp.estg.peddypapper.components.models.Question
import java.time.LocalTime
import java.util.Random
import kotlin.math.pow

// Easy question gives 5 exp
const val EASY_QUESTION_EXP = 5

// Medium question gives 10 exp
const val MEDIUM_QUESTION_EXP = 10

// Hard question gives 20 exp
const val HARD_QUESTION_EXP = 20

// Max distance to answer a question is 1 kilometer
const val MAX_DISTANCE_TO_ANSWER_QUESTION = 1000.0

// Acceleration threshold to gain experience
// If player is moving faster than 20 m/s^2, he gains experience
const val ACCURACY_THRESHOLD = 20f


/**
 * Component that represents the game of the application.
 *
 * It is the screen that the user sees when he clicks on the Game text in the navigation drawer.
 * It contains a Google Map that shows the user's location.
 *
 * Before the map is shown, the user must give permission to access his location.
 * if the user does not give permission, a text will be shown indicating that the permission was not given.
 *
 * If the user gives permission, the map will be shown.
 *
 * @param disableGesturesEnabled Function that disables the gestures of the navigation drawer.
 *
 * @see SetupGoogleMap
 */
@Composable
fun GameComponent(
    disableGesturesEnabled: () -> Unit,
    wasAnswerToQuestionCorrect: (Boolean) -> Unit,
    questionDifficulty: (String) -> Unit,
    playerInformation: PlayerInformation,
    increasePlayerExperience: (Int) -> Unit,
    ctx: Context
) {
    val permissionGiven = remember {
        mutableIntStateOf(0)
    }

    val locationStatus = remember {
        mutableStateOf(LatLng(41.14961, -8.61099))
    }

    if (
        ActivityCompat.checkSelfPermission(
            ctx,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
        ActivityCompat.checkSelfPermission(
            ctx,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    ) {
        permissionGiven.intValue = 2
    }

    val sensorManager = ctx.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    val stepCounterSensor: Sensor? = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
    val accelerometerSensor: Sensor? = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)

    val totalDistance = remember {
        mutableFloatStateOf(0f)
    }

    val permissionLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) {
                permissionGiven.intValue += 1
            }
        }

    LaunchedEffect(key1 = "Permission") {
        permissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        permissionLauncher.launch(Manifest.permission.ACCESS_COARSE_LOCATION)
    }

    DisposableEffect(Unit) {
        if (stepCounterSensor != null) {
            val stepCounterListener =
                object : TriggerEventListener() {
                    override fun onTrigger(event: TriggerEvent?) {
                        event.let {
                            val averageStepLength = 0.762f // Average step length in meters
                            val steps = event?.values?.get(0) ?: 0f
                            totalDistance.floatValue += steps * averageStepLength

                            if (totalDistance.floatValue >= 1000) {
                                increasePlayerExperience(5)
                                totalDistance.floatValue = 0f
                            }
                        }
                    }
                }

            sensorManager.requestTriggerSensor(
                stepCounterListener,
                stepCounterSensor
            )

            onDispose {
                sensorManager.cancelTriggerSensor(stepCounterListener, stepCounterSensor)
            }
        } else {
            val accelerometerListener =
                object : SensorEventListener {
                    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
                        // Do nothing
                    }

                    override fun onSensorChanged(event: SensorEvent?) {
                        if (event?.sensor?.type != Sensor.TYPE_ACCELEROMETER) {
                            return
                        }

                        val x = event.values?.get(0) ?: 0f
                        val y = event.values?.get(1) ?: 0f
                        val z = event.values?.get(2) ?: 0f

                        //Log.d("game accelerometer", "x: $x, y: $y, z: $z")

                        val acceleration =
                            (x * x + y * y + z * z) / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH)

                        //Log.d("game accelerometer", "acceleration: $acceleration")

                        if (acceleration.compareTo(ACCURACY_THRESHOLD) > 0) {
                            totalDistance.floatValue += acceleration
                        }

                        //Log.d("game accelerometer", "totalDistance: ${totalDistance.floatValue}")
                        if (totalDistance.floatValue >= 1000) {
                            increasePlayerExperience(5)
                            totalDistance.floatValue = 0f
                        }
                    }
                }

            sensorManager.registerListener(
                accelerometerListener,
                accelerometerSensor,
                SensorManager.SENSOR_DELAY_NORMAL
            )

            onDispose {
                sensorManager.unregisterListener(accelerometerListener)
            }
        }

        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(ctx)
        fusedLocationClient.lastLocation.addOnSuccessListener {
            locationStatus.value = LatLng(it.latitude, it.longitude)
        }.addOnFailureListener {
            locationStatus.value = LatLng(41.14961, -8.61099)
        }

        val locationRequest = LocationRequest.Builder(
            Priority.PRIORITY_HIGH_ACCURACY,
            10000,
        ).setMinUpdateIntervalMillis(30000).build()

        val locationCallback = object : LocationCallback() {
            override fun onLocationResult(locations: LocationResult) {
                for (location in locations.locations) {
                    locationStatus.value = LatLng(location.latitude, location.longitude)
                }
            }
        }

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)

        onDispose {
            fusedLocationClient.removeLocationUpdates(locationCallback)
        }
    }

    val markerLocations = remember {
        mutableStateOf(
            listOf(
                LatLng(locationStatus.value.latitude + 0.005, locationStatus.value.longitude + 0.005),
                LatLng(locationStatus.value.latitude + 0.015, locationStatus.value.longitude + 0.015),
                LatLng(locationStatus.value.latitude - 0.01, locationStatus.value.longitude - 0.01),
            )
        )
    }

    // If permission is given, show the map
    if (permissionGiven.intValue == 2) {
        val userLocation = locationStatus.value
        val intent = Intent(ctx, DistanceNotificationService::class.java)
            .putExtra(DistanceNotificationService.EXTRA_USER_LOCATION, userLocation)
            .putExtra(DistanceNotificationService.EXTRA_MARKER_LOCATIONS, ArrayList(markerLocations.value))
        ContextCompat.startForegroundService(ctx, intent)


        SetupGoogleMap(
            locationStatus.value,
            disableGesturesEnabled = disableGesturesEnabled,
            wasAnswerToQuestionCorrect = wasAnswerToQuestionCorrect,
            questionDifficulty = questionDifficulty,
            playerInformation = playerInformation,
        )
    } else {
        Text(stringResource(R.string.game_noPermission))
    }
}

/**
 * Component that represents the Google Map of the application.
 *
 * It is the map that is shown in the Game component after the user gives permission to access his location.
 *
 * @param location Location of the user.
 * @param disableGesturesEnabled Function that disables the gestures of the navigation drawer.
 */
@Composable
fun SetupGoogleMap(
    location: LatLng,
    disableGesturesEnabled: () -> Unit,
    wasAnswerToQuestionCorrect: (Boolean) -> Unit,
    questionDifficulty: (String) -> Unit,
    playerInformation: PlayerInformation
) {
    disableGesturesEnabled()

    val pseudoPlayerInformation = remember {
        mutableStateOf(playerInformation)
    }

    val wasQuestionAnsweredCorrectly = remember {
        mutableStateOf(false)
    }

    val questionDifficultyState = remember {
        mutableStateOf("")
    }

    val appViewModel: AppViewModel = viewModel()

    val questions = remember {
        mutableStateOf(listOf<Question>())
    }

    LaunchedEffect(wasQuestionAnsweredCorrectly.value) {
        if (wasQuestionAnsweredCorrectly.value) {
            pseudoPlayerInformation.value = calculateNewPlayerValues(
                currentUser = pseudoPlayerInformation.value,
                wasCorrect = wasQuestionAnsweredCorrectly.value,
                experienceToGainFromQuestion = calculateExperienceToGainFromQuestion(
                    questionDifficultyState.value
                )
            )
        }
    }

    LaunchedEffect(true) {
        appViewModel.getAllQuestions().asFlow().collect {
            val listOfQuestions = it.toMutableList()

            questions.value = listOfQuestions.shuffled(Random(System.currentTimeMillis()))
        }
    }

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        DisplayXpBar(
            playerInformation = pseudoPlayerInformation.value,
        )

        Box(
            modifier = Modifier.weight(1f)
        ) {
            DisplayMap(
                playerId = pseudoPlayerInformation.value.id,
                location = location,
                wasAnswerToQuestionCorrect = {
                    wasQuestionAnsweredCorrectly.value = it
                    wasAnswerToQuestionCorrect(it)
                },
                questionDifficulty = {
                    questionDifficultyState.value = it
                    questionDifficulty(it)
                },
                questions = questions.value
            )
        }
    }
}

/**
 * Function that draws an experience bar at the top of the screen.
 */
@Composable
fun DisplayXpBar(
    playerInformation: PlayerInformation
) {
    val currentExperience = rememberUpdatedState(playerInformation.gameExperience)
    val currentLevel = rememberUpdatedState(playerInformation.gameLevel)

    val currentLevelUpThreshold = rememberUpdatedState(
        (100 * (2.0.pow(
            currentLevel.value.minus(
                1
            )
        ))).toInt()
    )

    val currentExperiencePercentage = rememberUpdatedState(
        (currentExperience.value.toFloat()) / (currentLevelUpThreshold.value.toFloat())
    )

    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight(),
        shape = RoundedCornerShape(size = 10.dp),
        color = Color.White
    ) {
        LinearProgressIndicator(
            progress = currentExperiencePercentage.value,
            color = Color.Green,
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .padding(8.dp)
        )
    }
}

/**
 * Component that displays the map of the game.
 *
 * @param location Location of the user.
 * @param wasAnswerToQuestionCorrect Function that is called when the user answers a question.
 * @param questionDifficulty Function that is called when the user answers a question.
 */
@Composable
fun DisplayMap(
    playerId: Int,
    location: LatLng,
    wasAnswerToQuestionCorrect: (Boolean) -> Unit,
    questionDifficulty: (String) -> Unit,
    questions: List<Question>
) {
    val cameraPositionState = rememberCameraPositionState {
        position = CameraPosition.fromLatLngZoom(location, 15f)
    }

    val mapProperties = remember {
        mutableStateOf(
            MapProperties(
                maxZoomPreference = 30f, minZoomPreference = 5f
            )
        )
    }

    val mapUISettings = remember {
        mutableStateOf(
            MapUiSettings(
                mapToolbarEnabled = false,
                zoomGesturesEnabled = false,
            )
        )
    }

    val markerLocations = remember {
        mutableStateOf(
            listOf(
                LatLng(location.latitude + 0.005, location.longitude + 0.005),
                LatLng(location.latitude + 0.015, location.longitude + 0.015),
                LatLng(location.latitude - 0.01, location.longitude - 0.01),
            )
        )
    }

    val isUserTooFarAway = remember {
        mutableStateOf(
            false
        )
    }

    if (isUserTooFarAway.value) {
        Toast.makeText(
            LocalContext.current,
            stringResource(R.string.game_tooFarAway),
            Toast.LENGTH_SHORT
        ).show()

        isUserTooFarAway.value = false
    }

    GoogleMap(
        properties = mapProperties.value,
        uiSettings = mapUISettings.value,
        cameraPositionState = cameraPositionState,
    ) {
        MarkerInfoWindow(
            state = rememberMarkerState(position = location),
            onClick = {
                it.showInfoWindow()
                true
            },
            visible = true
        ) {
            Text(
                text = stringResource(R.string.game_userLocation),
                style = MaterialTheme.typography.bodyMedium
            )
        }

        markerLocations.value.forEachIndexed { index, markerLocation ->
            var dialogOpened by remember {
                mutableStateOf(false)
            }

            var wasQuestionAnswered by remember {
                mutableStateOf(false)
            }

            val distance = remember { mutableFloatStateOf(calculateDistanceBetweenUserAndMarker(location, markerLocation)) }

            val currentIndex = rememberUpdatedState(index)

            if (distance.floatValue <= MAX_DISTANCE_TO_ANSWER_QUESTION) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    DisplayIconAboveMarker(markerLocation, wasQuestionAnswered)
                }
            }

            MarkerInfoWindow(
                state = rememberMarkerState(position = markerLocation),
                onClick = {
                    if (distance.floatValue <= MAX_DISTANCE_TO_ANSWER_QUESTION) {
                        dialogOpened = true
                        it.showInfoWindow()
                    } else {
                        isUserTooFarAway.value = true
                    }

                    true
                },
                visible = !wasQuestionAnswered
            ) {
                if (dialogOpened) {
                    QuestionDialog(
                        playerId = playerId,
                        question = questions[currentIndex.value],
                        onDismissRequest = {
                            dialogOpened = false
                        },
                        wasAnswerToQuestionCorrect = {
                            wasQuestionAnswered = true
                            wasAnswerToQuestionCorrect(it)
                        },
                        questionDifficulty = questionDifficulty,
                    )
                    Text(
                        text = stringResource(R.string.game_userLocation),
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
            }
        }
    }
}


// You can customize the icon as per your requirement
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun DisplayIconAboveMarker(markerPosition: LatLng, wasQuestionAnswered: Boolean) {
    val scale = animateFloatAsState(
        targetValue = if (LocalTime.now().second % 3 < 1.5) 1.5f else 1f,
        animationSpec = infiniteRepeatable(
            animation = tween(1500, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        ), label = ""
    )

    Circle(
        center = markerPosition,
        zIndex = 2f,
        fillColor = Color(0x66FF0000),
        radius = scale.value * 65.0,
        visible = !wasQuestionAnswered,
        clickable = false,
        strokeColor = Color(0x66FF0000),
    )
}


/**
 * Component that represents the dialog that is shown when the user clicks on the marker of the map.
 *
 * @param onDismissRequest Function that is called when the dialog is dismissed.
 */
@Composable
fun QuestionDialog(
    playerId: Int,
    onDismissRequest: () -> Unit,
    wasAnswerToQuestionCorrect: (Boolean) -> Unit,
    questionDifficulty: (String) -> Unit,
    question: Question
) {
    Dialog(
        onDismissRequest = {
            onDismissRequest()
        },
        properties = DialogProperties(
            dismissOnBackPress = true,
            dismissOnClickOutside = true,
        )
    ) {
        var backgroundColor by remember {
            mutableStateOf("white")
        }

        val radioOptions = remember {
            mutableStateOf<List<String>>(listOf())
        }

        val incorrectAnswers = question.incorrectAnswers.split(",")

        val tempRadioOptions = mutableListOf<String>()

        tempRadioOptions.add(question.correctAnswer)
        tempRadioOptions.addAll(incorrectAnswers)

        radioOptions.value = tempRadioOptions.shuffled(Random(System.currentTimeMillis()))

        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            shape = RoundedCornerShape(size = 10.dp),
            color = when (backgroundColor) {
                "green" -> Color.Green
                "red" -> Color.Red
                else -> Color.White
            }
        ) {
            QuestionDisplayForm(
                playerId = playerId,
                notNullQuestion = question,
                changeQuestionBackgroundColor = {
                    backgroundColor = if (it) {
                        "green"
                    } else {
                        "red"
                    }
                },
                questionDifficulty = questionDifficulty,
                wasAnswerToQuestionCorrect = wasAnswerToQuestionCorrect,
                radioOptions = radioOptions
            )
        }
    }
}

@Composable
fun UpdatePlayerInformationInDatabase(
    currentUser: PlayerInformation,
    questionDifficulty: String,
    wasCorrect: Boolean
) {
    val appViewModel: AppViewModel = viewModel()

    // Calculate the experience to gain from the question
    val experienceToGainFromQuestion =
        calculateExperienceToGainFromQuestion(questionDifficulty)

    // Calculate the new player values
    val newPlayer = calculateNewPlayerValues(
        currentUser = currentUser,
        wasCorrect = wasCorrect,
        experienceToGainFromQuestion = experienceToGainFromQuestion
    )

    // Update the player
    LaunchedEffect(newPlayer) {
        Log.d("game", newPlayer.toString())
        appViewModel.updatePlayer(newPlayer)
    }
}
