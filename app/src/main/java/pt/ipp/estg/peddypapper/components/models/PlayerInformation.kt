package pt.ipp.estg.peddypapper.components.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PlayerInformation(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    val playerName: String = "",
    val nickname: String = "",
    val description: String = "",
    var gameLevel: Int = 1,
    var gameExperience: Int = 0,
    val score: Int = 0,
    var correctAnswers: Int = 0,
    var wrongAnswers: Int = 0,
    val email: String = "",
    val password: String = "",
    val totalQuestionsAnswered: Int = correctAnswers + wrongAnswers,
    var clanId: Int= 0,
    var phoneNumber: Int=0,
) {
constructor() : this(0, "", "", "", 1, 0, 0, 0, 0, "", "", 0, 0, 0)
}
