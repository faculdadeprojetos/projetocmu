package pt.ipp.estg.peddypapper.components.models

data class Answer(
    var id: Int,
    val playerId: Int,
    val question: String,
    val answer: String,
    val correctAnswer: String,
    val correct: Boolean,
) {
    constructor(): this(0, 0, "", "", "", false)
}
