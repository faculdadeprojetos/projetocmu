package pt.ipp.estg.peddypapper.components.composable

import android.content.Context
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.launch
import pt.ipp.estg.peddypapper.R
import pt.ipp.estg.peddypapper.components.data.AppViewModel
import pt.ipp.estg.peddypapper.components.models.PlayerInformation
import pt.ipp.estg.peddypapper.components.models.ReplyObject

/**
 * This is the account component.
 */
@Composable
fun AccountComponent(navController: NavHostController) {
    navController.navigate("login")
}

/**
 * This is the register form.
 */
@Composable
fun RegisterForm(navController: NavHostController, ctx: Context) {
    Card(
        colors = CardDefaults.cardColors(
            containerColor = Color.White,
            contentColor = Color.White,
        ),
        modifier = Modifier
            .padding(50.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        )
    ) {
        RegisterFormColumn(navController = navController, ctx = ctx)
    }
}

/**
 * This is the column for the register form.
 */
@Composable
fun RegisterFormColumn(modifier: Modifier = Modifier, navController: NavHostController, ctx: Context) {
    Column(
        modifier = modifier
            .verticalScroll(rememberScrollState())
            .padding(5.dp)
            .background(Color.White),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(R.string.account_register),
            fontSize = 30.sp,
            fontFamily = FontFamily.SansSerif,
            modifier = Modifier.padding(bottom = 20.dp),
            color = Color.Black
        )
        RegisterFormFields(navController = navController, ctx = ctx)
    }
}

/**
 * This is the fields of the form to register a new user.
 */
@Composable
fun RegisterFormFields(navController: NavHostController, ctx: Context) {
    // This is the scope for the coroutine.
    val scope = rememberCoroutineScope()

    // user variables
    var username by rememberSaveable { mutableStateOf("") }
    var nickname by rememberSaveable { mutableStateOf("") }
    var description by rememberSaveable { mutableStateOf("") }
    var email by rememberSaveable { mutableStateOf("") }
    var password by rememberSaveable { mutableStateOf("") }
    val level = remember { mutableIntStateOf(1) }
    val xp = remember { mutableIntStateOf(0) }
    val score = remember { mutableIntStateOf(0) }
    val rightAnswers = remember { mutableIntStateOf(0) }
    val wrongAnswers = remember { mutableIntStateOf(0) }
    val clanId = remember { mutableIntStateOf(0) }
    var phoneNumber by rememberSaveable { mutableStateOf("") }
    var passwordVisibility by rememberSaveable { mutableStateOf(false) }

    var hasRegisterBeenClicked by remember { mutableStateOf(false) }

    var notValidFormFields by remember { mutableStateOf("") }
    var openAlertDialog by remember { mutableStateOf(false) }

    if (hasRegisterBeenClicked) {
        // Validate the user information.
        if (notValidFormFields.isEmpty()) {
            if (username.length < 5 || username.length > 15) {
                notValidFormFields += "Username\n"
            }

            if (Patterns.EMAIL_ADDRESS.matcher(email).matches().not()) {
                notValidFormFields += "Email\n"
            }

            if (password.length < 8 || password.length > 16) {
                notValidFormFields += "Password\n"
            }
            if(phoneNumber.length !=9)
            notValidFormFields +=  "Phone Number\n"
        }

        if (notValidFormFields.isNotEmpty()) {
            if (!openAlertDialog) {
                ErrorDialog(
                    onDismissRequest = {
                        openAlertDialog = it
                        hasRegisterBeenClicked = false
                        notValidFormFields = ""
                    },
                    title = stringResource(R.string.account_register),
                    message = stringResource(R.string.account_registerInvalid),
                    notValidFields = notValidFormFields,
                    buttonText = stringResource(R.string.account_alertDialogClose),
                )
            }
        } else {
            // Verify if the user already exists before inserting it in the database.
            val appViewModel: AppViewModel = viewModel()

            val playerInformation = PlayerInformation(
                id = 0,
                playerName = username,
                nickname = nickname,
                description = description,
                email = email,
                password = password,
                gameLevel = level.intValue,
                gameExperience = xp.intValue,
                score = score.intValue,
                correctAnswers = rightAnswers.intValue,
                wrongAnswers = wrongAnswers.intValue,
                clanId = clanId.intValue,
                phoneNumber = phoneNumber.toInt()

            )

            Log.d("PlayerInformation", "Entered else")

            // Insert the user in the database.
            if (hasRegisterBeenClicked) {
                Log.d("PlayerInformation", "Entered hasRegisterBeenClicked")
                LaunchedEffect(Unit) {
                    Log.d("PlayerInformation", "Entered LaunchedEffect")

                    // Swapped logic to not use .observeAsState() because it kept calling the insertPlayer() method multiple times.
                    appViewModel.insertPlayer(playerInformation).asFlow()
                        .collect { result: ReplyObject ->
                            Log.d("PlayerInformation", "Entered collect")
                            Log.d("PlayerInformation", result.toString())

                            if (result.wasSuccessful) {
                                scope.launch {
                                    // Display a Toast indicating success.
                                    Toast.makeText(
                                        ctx,
                                        result.message,
                                        Toast.LENGTH_SHORT
                                    ).show()

                                    // Change the screen to the login screen.
                                    navController.navigate("login")
                                }
                            } else {
                                scope.launch {
                                    // Display a Toast indicating failure.
                                    Toast.makeText(
                                        ctx,
                                        result.message,
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        }
                }
            }

        }
    }

    /**
     * This is the text field for the username.
     */
    FormOutlinedTextField(
        value = username,
        onValueChange = {
            username = it
        },
        label = stringResource(R.string.account_usernameLabel),
        leadingIcon = {
            Text(
                text = "@",
                fontFamily = FontFamily.SansSerif,
                fontSize = 20.sp,
                color = Color.Black
            )
        },
        isInvalid = username.length < 5 || username.length > 15,
        errorMessage = stringResource(R.string.account_usernameInvalid)
    )

    /**
     * This is the text field for the nickname.
     */
    FormOutlinedTextField(
        value = nickname,
        onValueChange = {
            nickname = it
        },
        label = stringResource(R.string.account_nicknameLabel),
    )

    /**
     * This is the text field for the description.
     */
    FormOutlinedTextField(
        value = description,
        onValueChange = {
            description = it
        },
        label = stringResource(R.string.account_descriptionLabel),
    )

    /**
     * This is the text field for the email.
     */
    FormOutlinedTextField(
        value = email,
        onValueChange = {
            email = it
        },
        label = stringResource(R.string.account_emailLabel),
        leadingIcon = {
            Icon(
                Icons.Default.Email,
                contentDescription = stringResource(R.string.account_emailLabel),
                tint = Color.Black
            )
        },
        isInvalid = Patterns.EMAIL_ADDRESS.matcher(email).matches().not(),
        errorMessage = stringResource(R.string.account_emailInvalid) + " --> $email"
    )
    //Create a phone number field
    FormOutlinedTextField(
        value = phoneNumber,
        onValueChange = {
            phoneNumber = it
        },
        label = "Phone Number",
        leadingIcon = {
            Icon(
                Icons.Default.Phone,
                contentDescription = "Email",
                tint = Color.Black
            )
        },
    )

    /**
     * This is the text field for the password.
     */
    FormOutlinedTextField(
        value = password,
        onValueChange = { password = it },
        label = stringResource(R.string.account_passwordLabel),
        leadingIcon = {
            IconButton(
                onClick = { passwordVisibility = !passwordVisibility },
                content = {
                    Icon(
                        painter = if (passwordVisibility) painterResource(id = R.drawable.baseline_visibility_off_24) else painterResource(
                            id = R.drawable.baseline_visibility_24
                        ),
                        contentDescription = "Visibility",
                        tint = Color.Black
                    )
                }
            )
        },
        applyVisualTransformation = !passwordVisibility,
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Password,
            imeAction = ImeAction.Done
        ),
        isInvalid = password.length < 8 || password.length > 16,
        errorMessage = stringResource(R.string.account_passwordInvalid)
    )

    /**
     * This is the button to register the user.
     */
    Button(
        onClick = {
            hasRegisterBeenClicked = true
            openAlertDialog = false
        },
        content = {
            Text(
                text = stringResource(R.string.account_register),
                fontFamily = FontFamily.SansSerif,
                fontSize = 18.sp,
                color = Color.Black
            )
        },
    )
}

/**
 * This is the login form.
 */
@Composable
fun LoginForm(
    navController: NavHostController,
    changeCurrentUser: (PlayerInformation) -> Unit,
) {
    Card(
        colors = CardDefaults.cardColors(
            containerColor = Color.White,
            contentColor = Color.White,
        ),
        modifier = Modifier
            .padding(50.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        )
    ) {
        LoginFormColumn(navController = navController, changeCurrentUser = changeCurrentUser)
    }
}

/**
 * This is the column for the login form.
 */
@Composable
fun LoginFormColumn(
    modifier: Modifier = Modifier,
    navController: NavHostController,
    changeCurrentUser: (PlayerInformation) -> Unit,
) {
    Column(
        modifier = modifier
            .verticalScroll(rememberScrollState())
            .padding(5.dp)
            .background(Color.White),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(R.string.account_login),
            fontSize = 30.sp,
            fontFamily = FontFamily.SansSerif,
            modifier = Modifier.padding(bottom = 20.dp),
            color = Color.Black
        )

        LoginFormFields(navController = navController, changeCurrentUser = changeCurrentUser)

        AppClickableText(
            text = loginAnnotatedString(labelText = stringResource(R.string.account_register)),
            navController = navController,
            verticalPadding = 20.dp,
        )
    }
}

/**
 * This is the fields of the form to login a user.
 */
@Composable
fun LoginFormFields(
    navController: NavHostController,
    changeCurrentUser: (PlayerInformation) -> Unit
) {
    val scope = rememberCoroutineScope()

    var username by rememberSaveable { mutableStateOf("John Doe") }
    var password by rememberSaveable { mutableStateOf("12345678") }
    var passwordVisibility by rememberSaveable { mutableStateOf(false) }

    var hasLoginButtonBeenClicked by remember { mutableStateOf(false) }

    val appViewModel: AppViewModel = viewModel()

    if (hasLoginButtonBeenClicked) {
        Log.d("PlayerInformation", "Entered hasLoginButtonBeenClicked")
        LaunchedEffect(Unit) {
            Log.d("PlayerInformation", "Entered LaunchedEffect")
            appViewModel.getPlayerInformation(username, password).asFlow()
                .collect { playerInfo: PlayerInformation? ->
                    Log.d("PlayerInformation", "Entered collect")
                    Log.d("PlayerInformation", playerInfo.toString())
                    playerInfo?.let {
                        scope.launch {
                            // Change the current user.
                            changeCurrentUser(playerInfo)

                            Log.d("PlayerInformation", playerInfo.toString())

                            // Change the screen to the profile screen.
                            navController.navigate("playerProfile")
                        }
                    } ?: run {
                        // Player not found.
                        Log.d("PlayerInformation", "Player not found")
                        scope.launch {
                            // Navigate to a different screen or show a message to the user.
                            navController.navigate("register")
                        }
                    }
                }

        }
    }

    /**
     * This is the text field for the username.
     */
    FormOutlinedTextField(
        value = username,
        onValueChange = {
            username = it
        },
        label = stringResource(R.string.account_usernameLabel),
        leadingIcon = {
            Text(
                text = "@",
                fontFamily = FontFamily.SansSerif,
                fontSize = 20.sp,
                color = Color.Black
            )
        },
    )

    /**
     * This is the text field for the password.
     */
    FormOutlinedTextField(
        value = password,
        onValueChange = { password = it },
        label = stringResource(R.string.account_passwordLabel),
        leadingIcon = {
            IconButton(
                onClick = { passwordVisibility = !passwordVisibility },
                content = {
                    Icon(
                        painter = if (passwordVisibility) painterResource(id = R.drawable.baseline_visibility_off_24) else painterResource(
                            id = R.drawable.baseline_visibility_24
                        ),
                        contentDescription = "Visibility",
                        tint = Color.Black
                    )
                }
            )
        },
        applyVisualTransformation = !passwordVisibility,
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Password,
            imeAction = ImeAction.Done
        ),
    )

    /**
     * This is the button to login the user.
     */
    Button(
        onClick = {
            hasLoginButtonBeenClicked = true
        },
        content = {
            Text(
                text = stringResource(R.string.account_login),
                fontFamily = FontFamily.SansSerif,
                fontSize = 18.sp,
                color = Color.Black
            )
        },
    )
}

/**
 * This is the form to edit the user profile.
 */
@Composable
fun EditProfileForm(
    navController: NavHostController,
    player: PlayerInformation,
    changeCurrentUser: (PlayerInformation) -> Unit,
    ctx: Context
) {
    Card(
        colors = CardDefaults.cardColors(
            containerColor = Color.White,
            contentColor = Color.White,
        ),
        modifier = Modifier
            .padding(50.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        )
    ) {
        EditProfileFormColumn(
            navController = navController,
            player = player,
            changeCurrentUser = changeCurrentUser,
            ctx = ctx
        )
    }
}

/**
 * This is the column for the edit profile form.
 */
@Composable
fun EditProfileFormColumn(
    modifier: Modifier = Modifier,
    navController: NavHostController,
    player: PlayerInformation,
    changeCurrentUser: (PlayerInformation) -> Unit,
    ctx: Context
) {
    Column(
        modifier = modifier
            .verticalScroll(rememberScrollState())
            .padding(5.dp)
            .background(Color.White),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(R.string.account_editProfile),
            fontSize = 30.sp,
            fontFamily = FontFamily.SansSerif,
            modifier = Modifier.padding(bottom = 20.dp),
            color = Color.Black
        )

        EditProfileFormFields(
            navController = navController,
            player = player,
            changeCurrentUser = changeCurrentUser,
            ctx = ctx
        )
    }
}

/**
 * This is the fields of the form to edit the user profile.
 */
@Composable
fun EditProfileFormFields(
    navController: NavHostController,
    player: PlayerInformation,
    changeCurrentUser: (PlayerInformation) -> Unit,
    ctx: Context
) {
    val scope = rememberCoroutineScope()

    // user variables
    var username by rememberSaveable { mutableStateOf(player.playerName) }
    var nickname by rememberSaveable { mutableStateOf(player.nickname) }
    var description by rememberSaveable { mutableStateOf(player.description) }
    var phoneNumber by rememberSaveable { mutableStateOf(player.phoneNumber.toString()) }
    var email by rememberSaveable { mutableStateOf(player.email) }
    var password by rememberSaveable { mutableStateOf(player.password) }
    val level = remember { mutableIntStateOf(player.gameLevel) }
    val xp = remember { mutableIntStateOf(player.gameExperience) }
    val score = remember { mutableIntStateOf(player.score) }
    val rightAnswers = remember { mutableIntStateOf(player.correctAnswers) }
    val wrongAnswers = remember { mutableIntStateOf(player.wrongAnswers) }
    val clanId = remember { mutableIntStateOf(player.clanId) }

    var passwordVisibility by rememberSaveable { mutableStateOf(false) }

    var hasEditProfileButtonBeenClicked by remember { mutableStateOf(false) }

    var notValidFormFields by remember { mutableStateOf("") }
    var openAlertDialog by remember { mutableStateOf(false) }

    if (hasEditProfileButtonBeenClicked) {
        // Validate the user information.
        if (notValidFormFields.isEmpty()) {
            if (username.length < 5 || username.length > 15) {
                notValidFormFields += "Username\n"
            }

            if (Patterns.EMAIL_ADDRESS.matcher(email).matches().not()) {
                notValidFormFields += "Email\n"
            }

            if (password.length < 8 || password.length > 16) {
                notValidFormFields += "Password\n"
            }
        }

        if (notValidFormFields.isNotEmpty()) {
            if (!openAlertDialog) {
                ErrorDialog(
                    onDismissRequest = {
                        openAlertDialog = it
                        hasEditProfileButtonBeenClicked = false
                        notValidFormFields = ""
                    },
                    title = stringResource(R.string.account_editProfile),
                    message = stringResource(R.string.account_editProfileInvalid),
                    notValidFields = notValidFormFields,
                    buttonText = stringResource(R.string.account_alertDialogClose),
                )
            }
        } else {
            // Verify if the user already exists before inserting it in the database.
            val appViewModel: AppViewModel = viewModel()

            val playerInformation = PlayerInformation(
                id = player.id,
                playerName = username,
                nickname = nickname,
                description = description,
                phoneNumber = phoneNumber.toInt(),
                email = email,
                password = password,
                gameLevel = level.intValue,
                gameExperience = xp.intValue,
                score = score.intValue,
                correctAnswers = rightAnswers.intValue,
                wrongAnswers = wrongAnswers.intValue,
                clanId = clanId.intValue

            )

            Log.d("PlayerInformation", "Entered else")

            // Update the user in the database.
            if (hasEditProfileButtonBeenClicked) {
                Log.d("PlayerInformation", "Entered hasEditProfileButtonBeenClicked")
                LaunchedEffect(Unit) {
                    Log.d("PlayerInformation", "Entered LaunchedEffect")

                    // Swapped logic to not use .observeAsState() because it kept calling the insertPlayer() method multiple times.
                    appViewModel.updatePlayer(playerInformation).asFlow()
                        .collect { result: ReplyObject ->
                            Log.d("PlayerInformation", "Entered collect")
                            Log.d("PlayerInformation", result.toString())

                            if (result.wasSuccessful) {
                                scope.launch {
                                    // Display a Toast indicating success.
                                    Toast.makeText(
                                        ctx,
                                        result.message,
                                        Toast.LENGTH_SHORT
                                    ).show()

                                    // Update information of the current user for the rest of the app.
                                    changeCurrentUser(playerInformation)

                                    // Change the screen to the profile screen.
                                    navController.navigate("playerProfile")
                                }
                            } else {
                                scope.launch {
                                    // Display a Toast indicating failure.
                                    Toast.makeText(
                                        ctx,
                                        result.message,
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        }
                }
            }

        }
    }

    /**
     * This is the text field for the username.
     */
    FormOutlinedTextField(
        value = username,
        onValueChange = {
            username = it
        },
        label = stringResource(R.string.account_usernameLabel),
        leadingIcon = {
            Text(
                text = "@",
                fontFamily = FontFamily.SansSerif,
                fontSize = 20.sp,
                color = Color.Black
            )
        },
        isInvalid = username.length < 5 || username.length > 15,
        errorMessage = stringResource(R.string.account_usernameInvalid)
    )

    /**
     * This is the text field for the nickname.
     */
    FormOutlinedTextField(
        value = nickname,
        onValueChange = {
            nickname = it
        },
        label = stringResource(R.string.account_nicknameLabel),
    )

    /**
     * This is the text field for the description.
     */
    FormOutlinedTextField(
        value = description,
        onValueChange = {
            description = it
        },
        label = stringResource(R.string.account_descriptionLabel),
    )

    /**
     * This is the text field for the phone number.
     */
    FormOutlinedTextField(
        value = phoneNumber,
        onValueChange = {
            phoneNumber = it
        },
        label = stringResource(R.string.account_phoneNumberLabel),
        leadingIcon = {
            Icon(
                Icons.Default.Phone,
                contentDescription = "Email",
                tint = Color.Black
            )
        },
    )

    /**
     * This is the text field for the email.
     */
    FormOutlinedTextField(
        value = email,
        onValueChange = {
            email = it
        },
        label = stringResource(R.string.account_emailLabel),
        leadingIcon = {
            Icon(
                Icons.Default.Email,
                contentDescription = stringResource(R.string.account_emailLabel),
                tint = Color.Black
            )
        },
        isInvalid = Patterns.EMAIL_ADDRESS.matcher(email).matches().not(),
        errorMessage = stringResource(R.string.account_emailInvalid) + " --> $email"
    )

    /**
     * This is the text field for the password.
     */
    FormOutlinedTextField(
        value = password,
        onValueChange = { password = it },
        label = stringResource(R.string.account_passwordLabel),
        leadingIcon = {
            IconButton(
                onClick = { passwordVisibility = !passwordVisibility },
                content = {
                    Icon(
                        painter = if (passwordVisibility) painterResource(id = R.drawable.baseline_visibility_off_24) else painterResource(
                            id = R.drawable.baseline_visibility_24
                        ),
                        contentDescription = "Visibility",
                        tint = Color.Black
                    )
                }
            )
        },
        applyVisualTransformation = !passwordVisibility,
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Password,
            imeAction = ImeAction.Done
        ),
        isInvalid = password.length < 8 || password.length > 16,
        errorMessage = stringResource(R.string.account_passwordInvalid)
    )

    /**
     * This is the button to edit the user profile.
     */
    Button(
        onClick = {
            hasEditProfileButtonBeenClicked = true
            openAlertDialog = false
        },
        content = {
            Text(
                text = stringResource(R.string.account_editProfile),
                fontFamily = FontFamily.SansSerif,
                fontSize = 18.sp,
                color = Color.Black
            )
        },
    )
}

@Composable
fun FormOutlinedTextField(
    value: String,
    onValueChange: (String) -> Unit,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    label: String,
    applyVisualTransformation: Boolean = false,
    isInvalid: Boolean = false,
    errorMessage: String = "",
    leadingIcon: @Composable (() -> Unit)? = null,
) {
    OutlinedTextField(
        modifier = Modifier
            .padding(top = 10.dp, start = 30.dp, end = 30.dp, bottom = 20.dp)
            .background(Color.White),
        value = value,
        onValueChange = { onValueChange(it) },
        label = {
            Text(
                label,
                fontFamily = FontFamily.SansSerif,
                fontSize = 18.sp,
                color = Color.Black
            )
        },
        colors = OutlinedTextFieldDefaults.colors(
            cursorColor = Color.Black,
            focusedLabelColor = Color.Black,
            unfocusedLabelColor = Color.Black,
            focusedTextColor = Color.Black,
            unfocusedTextColor = Color.Black,
            focusedBorderColor = if (isInvalid) Color.Red else Color.Gray,
            unfocusedBorderColor = if (isInvalid) Color.Red else Color.Gray,
        ),
        keyboardOptions = keyboardOptions,
        visualTransformation = if (applyVisualTransformation) PasswordVisualTransformation() else VisualTransformation.None,
        supportingText = {
            if (isInvalid) {
                Text(
                    text = errorMessage,
                    fontFamily = FontFamily.SansSerif,
                    fontSize = 12.sp,
                    color = Color.Red
                )
            }
        },
        leadingIcon = leadingIcon
    )
}

@Composable
fun loginAnnotatedString(labelText: String): AnnotatedString {
    return buildAnnotatedString {
        withStyle(
            style = SpanStyle(
                color = MaterialTheme.colorScheme.primary,
                fontWeight = FontWeight.Bold,
                fontSize = 12.sp,
                fontFamily = FontFamily.SansSerif,
            )
        ) {
            append(labelText)
        }
    }
}

/**
 * Function to display the error dialog.
 *
 * @param onDismissRequest Function that is called when the dialog is dismissed.
 * @param title Title of the dialog.
 * @param message Message of the dialog.
 * @param buttonText Text of the button of the dialog.
 */
@Composable
fun ErrorDialog(
    onDismissRequest: (Boolean) -> Unit,
    title: String,
    message: String,
    notValidFields: String,
    buttonText: String,
) {
    Box(
        modifier = Modifier.fillMaxSize(),
    ) {
        Dialog(
            onDismissRequest = {
                onDismissRequest(false)
            },
            properties = DialogProperties(
                dismissOnBackPress = true,
                dismissOnClickOutside = true,
            )
        ) {
            Card(
                colors = CardDefaults.cardColors(
                    containerColor = Color.White,
                    contentColor = Color.White,
                ),
                modifier = Modifier
                    .padding(10.dp),
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 6.dp
                )
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Text(
                        text = title,
                        fontSize = 30.sp,
                        fontFamily = FontFamily.SansSerif,
                        modifier = Modifier.padding(bottom = 20.dp),
                        color = Color.Black
                    )

                    Text(
                        text = message,
                        fontSize = 18.sp,
                        fontFamily = FontFamily.SansSerif,
                        color = Color.Black,
                        modifier = Modifier.padding(start = 15.dp, bottom = 20.dp, end = 15.dp)
                    )

                    Text(
                        text = notValidFields,
                        fontSize = 18.sp,
                        fontFamily = FontFamily.SansSerif,
                        color = Color.Black
                    )

                    TextButton(
                        onClick = {
                            onDismissRequest(true)
                        },
                        content = {
                            Text(
                                text = buttonText,
                                fontFamily = FontFamily.SansSerif,
                                fontSize = 18.sp,
                                color = Color.Black
                            )
                        },
                    )
                }
            }
        }
    }
}

@Composable
fun RedirectToProfileIfLoggedIn(
    navController: NavHostController,
    currentUser: PlayerInformation?,
    changeCurrentUser: (PlayerInformation?) -> Unit
) {
    val appViewModel: AppViewModel = viewModel()

    val scope = rememberCoroutineScope()

    LaunchedEffect(currentUser) {
        Log.d("login", "Entered LaunchedEffect")
        if (currentUser != null) {
            appViewModel.getPlayerInformation(
                currentUser.playerName,
                currentUser.password
            ).asFlow()
                .collect { playerInfo: PlayerInformation? ->
                    playerInfo?.let {
                        Log.d("login", "Returned playerInfo not null")
                        scope.launch {
                            changeCurrentUser(playerInfo)
                            navController.navigate("playerProfile")
                        }
                    } ?: run {
                        Log.d("login", "Returned playerInfo null")
                        changeCurrentUser(null)
                    }
                }
        }
    }
}

@Preview
@Composable
fun PreviewRegisterForm() {
    RegisterForm(navController = rememberNavController(), ctx = LocalContext.current)
}



