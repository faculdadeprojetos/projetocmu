package pt.ipp.estg.peddypapper.components.composable

import android.content.Context
import android.content.res.Configuration
import android.os.LocaleList
import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import pt.ipp.estg.peddypapper.R
import java.util.Locale

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsComponent(ctx: Context) {
    var expanded by remember { mutableStateOf(false) }

    val portuguese = stringResource(R.string.settings_portuguese)

    val english = stringResource(R.string.settings_english)

    val listOfLanguages = listOf(english, portuguese)

    var selectedLanguage by remember { mutableStateOf(listOfLanguages[0]) }

    Column (
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 10.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(text = stringResource(R.string.settings_title), fontSize = 24.sp, fontFamily = FontFamily.SansSerif)

        Spacer(modifier = Modifier.padding(top = 26.dp))

        ExposedDropdownMenuBox(
            expanded = expanded,
            onExpandedChange = {
                expanded = it
            }
        ) {
            OutlinedTextField(
                modifier = Modifier.menuAnchor(),
                readOnly = true,
                value = selectedLanguage,
                onValueChange = {},
                label = {
                    Text(
                        stringResource(R.string.settings_languageLabel),
                        fontFamily = FontFamily.SansSerif,
                        fontSize = 16.sp,
                        color = Color.Black
                    )
                },
                trailingIcon = {
                    ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded)
                },
            )

            ExposedDropdownMenu(
                expanded = expanded,
                onDismissRequest = {
                    expanded = false
                }
            ) {
                DropdownMenuContent(
                    listOfLanguages,
                    selectedOption = {
                        selectedLanguage = it

                        when (selectedLanguage) {
                            portuguese -> {
                                changeLocale(ctx, "pt")
                            }

                            english -> {
                                changeLocale(ctx, "en")
                            }
                        }

                        expanded = false
                    }
                )
            }
        }
    }
}

@Composable
fun DropdownMenuContent(
    options: List<String>,
    selectedOption: (String) -> Unit
) {
    options.forEach { option ->
        DropdownMenuItem(
            onClick = { selectedOption(option) },
            modifier = Modifier
                .fillMaxWidth()
                .clickable { selectedOption(option) },
            text =
            {
                Text(
                    text = option,
                    color = Color.Black,
                    modifier = Modifier.padding(10.dp)
                )
            }
        )

    }
}

/**
 * Change the locale of the application
 */
private fun changeLocale(ctx: Context, language: String) {
    val locale = Locale(language)
    Locale.setDefault(locale)

    val configuration = Configuration(ctx.resources.configuration)
    configuration.setLocale(locale)

    val newContext = ctx.createConfigurationContext(configuration)

    // Update the context of the activity
    val resources = newContext.resources
    val metrics = resources.displayMetrics
    val config = resources.configuration
    config.setLocales(LocaleList(locale))
    resources.updateConfiguration(config, metrics)
}
