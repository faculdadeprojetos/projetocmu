package pt.ipp.estg.peddypapper.components.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import pt.ipp.estg.peddypapper.components.models.PlayerInformation

@Dao
interface PlayerDao {
    /**
     * Function to get all player information from firebase - Might not be used
     */
    @Query("SELECT * FROM playerinformation")
    fun getAll(): LiveData<List<PlayerInformation>>

    /**
     * Function to get player information by id from firebase
     */
    @Query("SELECT * FROM playerinformation WHERE id IN (:playerId)")
    fun getPlayerById(playerId: Int): LiveData<PlayerInformation>

    /**
     * Function to get player information by username and password from local cache
     */
    @Query("SELECT * FROM playerinformation WHERE playerName = :username AND password = :password LIMIT 1")
    fun getPlayerInformation(username: String, password: String): LiveData<PlayerInformation>

    /**
     * Function to insert player information into local cache
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPlayer(playerInformation: PlayerInformation)

    /**
     * Function to update local cache of player information
     */
    @Update
    suspend fun updatePlayer(playerInformation: PlayerInformation)

    /**
     * Function to delete local cache of player information
     */
    @Delete
    suspend fun deletePlayer(playerInformation: PlayerInformation)
}