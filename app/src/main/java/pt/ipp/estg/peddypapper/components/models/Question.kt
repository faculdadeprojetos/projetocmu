package pt.ipp.estg.peddypapper.components.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Question(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val question: String,
    val correctAnswer: String,
    val incorrectAnswers: String,
    val difficulty : String,
)
