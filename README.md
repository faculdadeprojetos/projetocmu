# Peddy Paper App

## Overview

Welcome to the Peddy Paper App! This mobile application is designed for organizing and participating in quizzes, providing an interactive and enjoyable experience for users.

## Project Setup

Before running the Peddy Paper App, make sure your development environment meets the following requirements:

- **Android Gradle Plugin Version:** 8.1.2
- **Gradle Version:** 8.0
- **JDK/SDK (whichever gradle says it needs) Version:** 17.0.5

## Troubleshooting

### Map Marker Display Issues

If you encounter any issues with the map not displaying markers, please follow these steps:

1. Uninstall the app from your device.
2. Reinstall the app.

